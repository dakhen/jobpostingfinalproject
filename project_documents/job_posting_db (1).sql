-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2020 at 12:36 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `job_posting_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`id`, `name`, `email`, `phone`, `logo`, `address`, `description`, `created_at`, `updated_at`) VALUES
(1, 'UP Job ', 'info@example.com', '123123', '', 'A108 Adam Street \r\nNew York, NY 535022\r\nUnited States ', 'Cras fermentum odio eu feugiat lide par naso tierr...\r\n', '2020-09-26 17:58:51', NULL),
(2, 'UP Job ', 'info@example.com', '012232323', '', 'A108 Adam Street  New York, NY 535022 United States ', 'Cras fermentum odio eu feugiat lide par naso tierr...', '2020-09-27 00:03:41', '2020-09-27 00:03:41'),
(3, 'UP Job ', 'info@example.com', '012232323', '', 'A108 Adam Street  New York, NY 535022 United States ', 'Cras fermentum odio eu feugiat lide par naso tierr...', '2020-09-27 00:16:40', '2020-09-27 00:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `can_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `can_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `work_experience` int(11) DEFAULT NULL,
  `professional_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hobbies` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `self_evaluate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `can_name`, `can_email`, `password`, `phone_num`, `age`, `work_experience`, `professional_level`, `education`, `language`, `hobbies`, `self_evaluate`, `user_id`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Sai', 'sai@gmail.com', '123', '123456', 12, 12, 'Medium', 'Bachelor', 'Khmer and English', 'Research', 'Fast learner', NULL, NULL, '2020-10-02 17:00:00', '2020-10-02 17:00:00'),
(2, 'dakhen', 'dakhen@gmail.com', 'hello@123', '0123456', 23, 3, 'Medium', 'Bachelor', 'English', 'IT', 'N/A', 2, NULL, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(3, 'Neji', 'neji@gmail.com', 'hello@123', '0123465', 25, 5, 'Advance', 'Master', 'English', 'NA', 'NA', 9, NULL, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(4, 'Chou', 'chou@gmail.com', 'hello@123', '0123456', 23, 3, 'Medium', 'Bachelor', 'Khmer and English', 'Research', 'Fast learner', 12, NULL, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(5, 'admin', 'admin2@example.com', 'admin@123', '01235489', 28, 10, 'Medium', 'PHD', 'English', 'IT', 'N/a', 11, NULL, '2020-10-10 17:00:00', '2020-10-10 17:00:00'),
(6, 'sun', 'sun@gmail.com', 'sun@123', '01235489', 21, 4, 'Medium', 'Master', 'Khmer and English', 'Research', 'Fast learner', 15, NULL, '2020-10-12 17:00:00', '2020-10-12 17:00:00'),
(7, 'amara', 'N/A', 'N/A', '01235489', 20, 2, 'Advance', 'Bachelor', 'Khmer and English', 'Research', 'Fast learner', 16, NULL, '2020-10-14 17:00:00', '2020-10-14 17:00:00'),
(8, 'Zed', 'zed@gmail.com', 'N/A', '01235489', 23, 5, 'Medium', 'Bachelor', 'Khmer and English', 'Research', 'Fast learner', 17, NULL, '2020-10-14 17:00:00', '2020-10-14 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_jobs`
--

CREATE TABLE `candidate_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_jobs`
--

INSERT INTO `candidate_jobs` (`id`, `user_id`, `job_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL, '2020-10-04 17:00:00'),
(2, 2, 5, NULL, '2020-10-04 17:00:00'),
(3, 2, 1, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(4, 2, 1, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(5, 9, 1, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(6, 9, 1, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(7, 12, 1, '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(8, 2, 3, '2020-10-10 17:00:00', '2020-10-10 17:00:00'),
(9, 11, 1, '2020-10-10 17:00:00', '2020-10-10 17:00:00'),
(10, 11, 4, '2020-10-10 17:00:00', '2020-10-10 17:00:00'),
(11, 15, 3, '2020-10-12 17:00:00', '2020-10-12 17:00:00'),
(12, 2, 1, '2020-10-14 17:00:00', '2020-10-14 17:00:00'),
(13, 17, 1, '2020-10-14 17:00:00', '2020-10-14 17:00:00'),
(14, 2, 12, '2020-10-14 17:00:00', '2020-10-14 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `created_at`, `updated_at`) VALUES
(1, 'IT', '2020-10-03 16:51:04', '2020-10-03 16:51:04'),
(2, 'cashier', '2020-10-14 10:05:39', '2020-10-14 10:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_mission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_vision` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_emp_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `company_mission`, `company_vision`, `company_type`, `company_emp_num`, `company_website`, `company_contact`, `user_id`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'The Venture', 'N/A', 'N/A', 'IT', '20->30', 'venture.com', '123123', 2, 0, '2020-10-03 16:48:35', '2020-10-03 16:48:35'),
(2, 'The Venture', 'N/A', 'N/A', 'IT', '20->30', 'venture.com', '123123', 2, 0, '2020-10-03 16:48:35', '2020-10-03 16:48:35'),
(3, 'amara company', 'n/a', 'n/a', 'bank', '20 -> 30', 'no', '0123654825', 16, NULL, '2020-10-14 10:04:43', '2020-10-14 10:04:43');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 'dakhen', 'dakhen@gmail.com', '0123456789', 'test from dakhen', 'test', '2020-10-05 07:38:11', '2020-10-05 07:38:11'),
(2, 'dakhen', 'dakhen@gmail.com', '0123456789', 'test from dakhen', 'test', '2020-10-05 07:38:24', '2020-10-05 07:38:24'),
(3, 'test', 'Test@gmail.com', '0123456789', 'test from dakhen', 'testtest', '2020-10-05 07:45:55', '2020-10-05 07:45:55'),
(4, 'test', 'dakhen@gmail.com', '0123456789', 'test from dakhen', 'tset', '2020-10-05 07:51:18', '2020-10-05 07:51:18'),
(5, 'dakhen', 'dakhen@gmail.com', '0123456789', 'test from dakhen', '12312', '2020-10-05 07:58:54', '2020-10-05 07:58:54'),
(6, 'dakhen', 'dakhen.soks@gmail.com', '0123456789', 'Dakhen Test', 'Hello, test', '2020-10-13 04:30:25', '2020-10-13 04:30:25'),
(7, 'dakhen', 'dakhen.soks@gmail.com', '0123456789', 'Dakhen Testing sms', 'testing', '2020-10-14 09:57:46', '2020-10-14 09:57:46');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_requirement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_salary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_closed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `can_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `job_name`, `job_description`, `job_requirement`, `job_location`, `job_salary`, `job_closed`, `cat_id`, `com_id`, `user_id`, `can_id`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Mobile Developer123', 'testingww', 'testingww', 'test2', 'Negotiable', '2020-10-21', 1, 1, NULL, 6, NULL, '2020-10-01 17:00:00', '2020-10-14 17:00:00'),
(10, 'Cashier', 'cashier on bank transaction', 'cashier on bank transaction', 'Phnom Penh', 'Negotiable', '2020-10-31', 2, 3, 16, NULL, NULL, '2020-10-13 17:00:00', '2020-10-14 17:00:00'),
(11, 'PHP Developer', 'testing', 'testing', 'testing', '1K', '2020-10-28', 2, 3, 2, NULL, NULL, '2020-10-13 17:00:00', '2020-10-14 17:00:00'),
(12, 'Mobile Developer', 'testing', 'testing', 'test', '1k', '2020-10-23', 1, 3, 2, NULL, NULL, '2020-10-14 17:00:00', '2020-10-14 17:00:00'),
(13, 'Java Developer', 'test', 'test', 'test', 'Negotiable', '2020-10-29', 1, 2, 2, NULL, NULL, '2020-10-14 17:00:00', '2020-10-14 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_24_085802_create_jobs_table', 2),
(5, '2020_09_24_092503_create_companies_table', 3),
(6, '2020_09_24_092931_create_categories_table', 3),
(7, '2020_09_24_092959_create_candidates_table', 3),
(8, '2020_09_24_093019_create_roles_table', 3),
(9, '2020_09_26_174030_create_aboutus_table', 4),
(10, '2020_09_26_174354_create_ourservices_table', 4),
(11, '2020_10_05_122620_add_user_id_to_candidates_table', 5),
(12, '2020_10_05_140521_create_contacts_table', 6),
(13, '2020_10_05_171920_create_candidate_jobs_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `ourservices`
--

CREATE TABLE `ourservices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ourservices`
--

INSERT INTO `ourservices` (`id`, `title`, `icon`, `description`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum Delino', 'ion-ios-bookmarks-outline', 'Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident', '#', '2020-09-26 18:13:51', NULL),
(2, 'Lorem Ipsum Delino', 'ion-ios-bookmarks-outline', 'Voluptatum deleniti atque corrupti quos dolores et...', '#', '2020-09-27 00:14:31', '2020-09-27 00:14:31'),
(3, 'Lorem Ipsum Delino', 'ion-ios-bookmarks-outline', 'Voluptatum deleniti atque corrupti quos dolores et...', '#', '2020-09-27 00:16:40', '2020-09-27 00:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2020-10-04 21:47:23', '2020-10-04 21:47:23'),
(2, 'Employer', '2020-10-04 21:47:39', '2020-10-04 21:47:39'),
(3, 'Candidate', '2020-10-04 21:47:45', '2020-10-04 21:47:45'),
(4, 'Admin', '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(5, 'Employee', '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(6, 'Candidate', '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(7, 'Admin', '2020-10-04 17:00:00', '2020-10-04 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `photo`, `is_active`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$IUSAQwMdem7qWcCcS1zxcuPVAZIgmgr.el6Fy2jxbFgI63/mD3fJe', NULL, NULL, NULL, NULL, '2020-09-08 09:16:46', '2020-09-08 09:16:46'),
(2, 'dakhen', 'dakhen@gmail.com', NULL, '$2y$10$FtKY..J9IxPQJoZCB/RdG.rfav/PXyTJ.K4sWaNvV4FTWISwyft06', NULL, NULL, 1, NULL, '2020-09-26 00:03:38', '2020-09-26 00:03:38'),
(3, 'sama', 'sama@gmail.com', NULL, '$2y$10$MKDXA6lotboskDP.78nCu.xqoPHzcHo3hRqf6l/ltd4spiNSjQUAq', NULL, NULL, NULL, NULL, '2020-10-04 21:55:07', '2020-10-04 21:55:07'),
(6, 'den', 'den@gmail.com', NULL, '$2y$10$ruKBXv9M1Kw0j95hffA8G.W25yoRuxWaUkOsripI8WQnIH0hd0YWG', NULL, NULL, 3, NULL, '2020-10-05 00:29:20', NULL),
(8, 'sen', 'sen@gmail.com', NULL, '$2y$10$0EP48/CkTs3n4gvXaiGJseVeyF42aDHapncl4fjZVleir1kbX.zM6', NULL, NULL, 3, NULL, '2020-10-05 00:30:23', NULL),
(9, 'neji', 'neji@gmail.com', NULL, '$2y$10$1ly64dZl.6r7fQURmoXjTeCuEhJYXeeuBFtcmGiXEs8ZOSwP/wqWu', NULL, NULL, 2, NULL, '2020-10-05 01:00:24', '2020-10-05 01:00:24'),
(10, 'Admin', 'admin@example.com', NULL, 'admin@123', '', NULL, 1, '', '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(11, 'Admin', 'admin2@example.com', NULL, '$2y$10$sfVNOTvl7D0MR1itAcTm3OresHVhswjq0dlKSas99I5akl9RzWK4O', '', NULL, 1, '', '2020-10-04 17:00:00', '2020-10-04 17:00:00'),
(12, 'chou', 'chou@gmail.com', NULL, '$2y$10$n3Hm0DH9KhWd6r9FJNSDJeWzcKRojmapU.4EpiRnGe/fK7oDitaWO', NULL, NULL, 3, NULL, '2020-10-05 10:49:53', NULL),
(13, 'benjamin', 'ben@gmail.com', NULL, '$2y$10$CgGZLD8Z/ED4T8U2b26LR.9peVfAvMFACn33cPAHrnpF2cx3UHuHu', NULL, NULL, 2, NULL, '2020-10-12 23:36:23', '2020-10-12 23:36:23'),
(14, 'jim', 'jim@gmail.com', NULL, '$2y$10$jHTJVzoiVfWkUduBHTBGNuUkGd6LAQJEdRs.lTCKXTJ6f1U/tGqdi', NULL, NULL, 3, NULL, '2020-10-12 23:36:52', NULL),
(15, 'sun', 'sun@gmail.com', NULL, '$2y$10$iybhkHmqk7oztS/1khr/vuJnhGmAqzwa2/gSDgqUNZeGlWwDgSyFi', NULL, NULL, 3, NULL, '2020-10-13 05:49:18', NULL),
(16, 'amara', 'arama@gmail.com', NULL, '$2y$10$cZMMaDIEkM173dprJ3db1e05o4gJv.PWyQDRRrVzf6d7vnZHkrXBq', NULL, NULL, 2, NULL, '2020-10-14 10:03:34', '2020-10-14 10:03:34'),
(17, 'zed', 'zed@gmail.com', NULL, '$2y$10$z4ZowoVY7QdKsyJlG9m7q.g9cNJnZU33PfDzrwnWS9ornPI3Bnr8e', NULL, NULL, 3, NULL, '2020-10-15 00:05:37', NULL),
(20, 'khenji1', 'khenji1@gmail.com', NULL, '$2y$10$RbNFzYnQT6VfCHSx/QrfxeOHdl6RuB43GXJxUqljAMO6VLW/XcUlO', NULL, NULL, NULL, NULL, '2020-10-15 02:18:39', '2020-10-15 02:18:39'),
(21, 'khenji2', 'khenji2@gmail.com', NULL, '$2y$10$IhLgJDMjlk1F7kMEpDaLXeexcGOIxL9aWAXJwyaIHIiKyCmf44/ZS', NULL, NULL, NULL, NULL, '2020-10-15 02:20:29', '2020-10-15 02:20:29'),
(22, 'khenji3', 'khenji3@gmail.com', NULL, '$2y$10$wdvG.bhYCMhT9gmGwCFIHu5gZNKEAqG3be3Uv8dQXQknKPRZB8muK', NULL, NULL, NULL, NULL, '2020-10-15 02:21:23', '2020-10-15 02:21:23'),
(23, 'khenji4', 'khenji4@gmail.com', NULL, '$2y$10$/2zx8uLWa9oykHVjP3lbZ.GiykAhuEvXcrjeFTQWT.MkW4qqtt2CK', NULL, NULL, NULL, NULL, '2020-10-15 02:24:06', '2020-10-15 02:24:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidate_jobs`
--
ALTER TABLE `candidate_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ourservices`
--
ALTER TABLE `ourservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `candidate_jobs`
--
ALTER TABLE `candidate_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `ourservices`
--
ALTER TABLE `ourservices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
