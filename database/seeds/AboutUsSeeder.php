<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('aboutus')->insert([
            'id' => 1,
            'name' => 'UP Job ',
            'email' => 'info@example.com',
            'phone' => '012232323',
            'logo' => '',
            'address' => 'A108 Adam Street  New York, NY 535022 United States ',
            'description'=> 'Cras fermentum odio eu feugiat lide par naso tierr...',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
