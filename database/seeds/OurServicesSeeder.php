<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OurServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ourservices')->insert([
            'id' => 1,
            'title' => 'Lorem Ipsum Delino',
            'icon' => 'ion-ios-bookmarks-outline',
            'description' => 'Voluptatum deleniti atque corrupti quos dolores et...',
            'link' => '#',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
