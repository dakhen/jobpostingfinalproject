<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        // $this->call(AboutUsSeeder::class);
        $this->call([
            AboutUsSeeder::class,
            OurServicesSeeder::class,
            UserSeeder::class,
            RoleSeeder::class,
            CategorySeeder::class
        ]);
    }
}
