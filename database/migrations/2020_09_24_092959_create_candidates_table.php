<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->string("can_name");
            $table->string("can_email");
            $table->string("password");
            $table->string("phone_num");
            $table->integer("age");
            $table->integer("work_experience")->nullable();
            $table->string("professional_level")->nullable();
            $table->string("education");
            $table->string("language");
            $table->string("hobbies")->nullable();
            $table->string("self_evaluate")->nullable();
            $table->tinyInteger("is_deleted")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
