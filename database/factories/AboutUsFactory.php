<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\AboutUs;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(AboutUs::class, function (Faker $faker) {
    return [
        'name' => 'UP Job ',
        'email' => 'info@example.com',
        'phone' => '012232323',
        'logo' => '',
        'address' => 'A108 Adam Street  New York, NY 535022 United States ',
        'description'=> 'Cras fermentum odio eu feugiat lide par naso tierr...',
        'create_at' => Carbon::now()
    ];
});
