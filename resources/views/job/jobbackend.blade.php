@extends('layouts.master')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <button class="btn btn-success pull-right dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download" ></i> Download
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        &nbsp;<a href="{{url('job/exportexcel')}}" style="text-decoration: none;"><i class="fa fa-file-excel-o" ></i> <b>Excel</b> file</a><br>
                        &nbsp;<a href="{{url('job/exportword')}}" style="text-decoration: none;"><i class="fa fa-file-word-o" ></i> <b>Word</b> file</a><br>
                        &nbsp;<a href="{{url('job/exportpdf')}}" style="text-decoration: none;"><i class="fa fa-file-pdf-o" ></i> <b>Pdf</b> file</a><br>
                    </div><br><br>
                  
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Job Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if (!$pagex) {
                            $pagex = 1;
                            }
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($joblists as $joblist)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $joblist->job_name }}</td>
                                    <td>
                                        <a href="{{ url('job/applyjob?id=' . $joblist->id. '&userid='. Auth::user()->id) }}"
                                            class="text-primary" title="Apply Job"
                                            onclick="return confirm('Are you sure to Apply?')">
                                            <span><i class="fa fa-check-circle"></i> </span>
                                        </a>
                                        <a href="{{ url('job/detailjob/' . $joblist->id) }}"
                                            class="text-primary" title="Job Detail">
                                            <span><i class="fa fa-info-circle"></i> </span>
                                        </a>
                                        @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                        <a href="{{ url('job/editjob/' . $joblist->id) }}"
                                            class="text-success" title="Update Job">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ url('job/allappliedjob/' . $joblist->id) }}"
                                            class="text-warning" title="Candiates Applied">
                                            <span><i class="fa fa-list-alt"></i> </span>
                                        </a>
                                        @endif
                                        @if (Auth::user()->role_id == 1)
                                        <a href="{{ url('job/deletejob?id=' . $joblist->id) }}"
                                            class="text-danger" title="Delete Job"
                                            onclick="return confirm('Are you sure to delete?')">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        @endif
                                    </td>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>

        </div>
    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_job").addClass("active open");
            $("#job_collapse").addClass("collapse in");
            $("#menu_job_list").addClass("active");

        });

    </script>
@endsection
