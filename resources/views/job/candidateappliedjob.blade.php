@extends('layouts.master')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                  
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Candidate Name</th>
                                <th>Candidate Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if (!$pagex) {
                            $pagex = 1;
                            }
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($allcandidates as $candidate)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $candidate->name }}</td>
                                    <td>{{ $candidate->email }}</td>
                                    <td>
                                        <a href="{{ url('/candidate/detailCandidate/'.$candidate->id) }}"
                                            class="text-primary" title="Candidate Detail">
                                            <span><i class="fa fa-info-circle"></i> </span>
                                        </a>
                                        {{-- <a href="{{ url('job/detailjob/' . $candidate->id) }}"
                                            class="text-primary" title="Job Detail">
                                            <span><i class="fa fa-info-circle"></i> </span>
                                        </a> --}}
                                        {{-- @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                        <a href="{{ url('job/editjob/' . $candidate->id) }}"
                                            class="text-success" title="Update Job">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @endif --}}
                                    </td>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>

        </div>
    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_job").addClass("active open");
            $("#job_collapse").addClass("collapse in");
            $("#menu_job_list").addClass("active");

        });

    </script>
@endsection
