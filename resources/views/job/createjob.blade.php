@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-gray">
                    <div class="card-block">
                        <h5 style="text-align: center;"><b>Post New Job</b></h5>
                        <hr>
                        <form action="{{ url('/job/savejob') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-9">
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <p>
                                                    {{ session('success') }}
                                                </p>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif

                                        <div class="form-group row">
                                            <label for="jobname" class="col-sm-4">Job Title
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="jobname" name='JobName' 
                                                    autofocus value="{{ old('jobname') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="jd" class="col-sm-4">Job Description
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <textarea type="text" class="form-control" id="jd" name='JobDescription'    
                                                    value="{{ old('jd') }}"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="jr" class="col-sm-4">Job Requirement
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <textarea type="text" class="form-control" id="jr" name='JobRequirement'    
                                                    value="{{ old('jr') }}"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="joblocation" class="col-sm-4">Job Location
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="joblocation" name='JobLocation'
                                                     value="{{ old('joblocation') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="jobsalary" class="col-sm-4">Job Salary
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="jobsalary" name='JobSalary'
                                                     value="{{ old('jobsalary') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="catid" class="col-sm-4">Category Type
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <select name="Category" id="category" class="form-control">
                                                    <option value="">--Select--</option>
                                                    @foreach ($categories as $cat)
                                                        <option value="{{ $cat->id }}">{{ $cat->cat_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="companyid" class="col-sm-4">Company
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <select name="Company" id="companyname" class="form-control">
                                                    <option value="">--Select--</option>
                                                    @foreach ($companies as $com)
                                                        <option value="{{ $com->id }}">{{ $com->company_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="jbclosedate" class="col-sm-4">Close Date
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="date" class="form-control" id="jbclosedate" name='JobCloseDate'
                                                     value="{{ old('jbclosedate') }}">
                                            </div>
                                        </div>
                                        <div style="display: none">
                                            <input type="text" name="userid" id="userid" value="{{ Auth::user()->id }}">
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button class="btn btn-primary btn-oval">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                                <a href="{{ url('/job/backendjobs') }}" class="btn btn-primary btn-oval">
                                                    <i class="fa fa-reply"></i> Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_job").addClass("active open");
            $("#job_collapse").addClass("collapse in");
            $("#menu_jobs").addClass("active");

        });

        function loadPhoto(e) {
            var img = document.getElementById('img');
            img.src = URL.createObjectURL(e.target.files[0]);
        }

    </script>
@endsection
{{-- @section('script')
<script>
    $(document).ready(function() {
        $("#sidebar-menu li ").removeClass("active open");
        $("#sidebar-menu li ul li").removeClass("active");

        $("#menu_company").addClass("active open");
        $("#company_collapse").addClass("collapse in");
        $("#menu_user").addClass("active");

    });

    function loadPhoto(e) {
        var img = document.getElementById('img');
        img.src = URL.createObjectURL(e.target.files[0]);
    }

</script>
@endsection --}}
