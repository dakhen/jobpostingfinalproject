@extends('layouts.front')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content" style="margin-bottom: 5%;">

        <!-- Start Re-exam working efford -->
        <div class="container-fluid">
            <div class="panel panel-default">
                <div class="panel-heading panel-info">
                    <h3 class="panel-title "><b>Search Job</b></h3>
                </div>
                <div class="panel-body">
                <form action="{{ url('/job/searchjob') }}"  method="GET">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="inputState">Position</label>
                            <input type="text" class="form-control" placeholder="enter position" id="position" name='position' />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputState">Category Name</label>
                            <select id="inputState" class="form-control" name="category">
                                <option value="">--Select--<option>

                                @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->cat_name}}<option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group col-md-3"><br>
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                       
                    </div>

                </form>
                    <br>
                    @foreach ($joblists as $joblist)
                    <div class="jobs" >
                    <a href="{{ url('job/viewdetailjob/' . $joblist->id) }}" style="display: block;">

                        <div class="row job_listing" style="margin-right: 0px;margin-left: 0px;">
                            <div class="col-md-3 company-info">
                            <div class="company-name mainf">
                                <a href="#">
                                    <img src="../{{$joblist->image}}" alt="Photo {{$joblist->company_name}}" class="img-responsive company-logo"><br><br>
                                    <span><a href="{{ url('job/viewdetailjob/' . $joblist->id) }}" class="ComDescription">{{ $joblist->company_name }}</a></span>
                                </a>
                            </div>        
                            </div>

                            <div class="col-md-9 job-info">
                            <div class="job-details">
                                    <div class="job_detail_page">
                                        <div class="row">

                                            <div class="job-salary col-md-4">
                                                <label for="">Position : </label>
                                                <span>{{ $joblist->job_name }}</span> 
                                            </div>

                                            <div class="job-salary col-md-4">
                                                <label for="">Salary : </label>
                                                <span>{{ $joblist->job_salary }}$</span> 

                                            </div>

                                            <div class="job-salary col-md-4">
                                                <label for="">Category : </label>
                                                <span>{{ $joblist->cat_name }}</span> 

                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="job-salary col-md-4">
                                                <label for="">Location : </label>
                                                <span>{{ $joblist->job_location }}</span> 

                                            </div>
                                            <div class="job-salary col-md-4">
                                                <label for="">Close Date : </label>
                                                <span style="color: red;">{{ $joblist->job_closed }}</span>  
                                            </div>

                                            <div class="job-salary col-md-4">
                                                <label for="">Job Type : </label>
                                                <span>{{ $joblist->job_location }}</span>  

                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="job-salary col-md-4">
                                                <label for="">Job Description : </label>
                                                <span>{{ $joblist->job_description }}</span> 

                                            </div>
                                            <div class="job-salary col-md-4">
                                                <label for="">Job Requirement : </label>
                                                <span>{{ $joblist->job_requirement }}</span>  
                                            </div>

                                            <div class="job-salary col-md-4">
                                                <label for="">Company Name : </label>
                                                <span>{{ $joblist->company_name }}</span>  

                                            </div>

                                        </div>
                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                        
                    </div>
                    <br>
                    @endforeach

                </div>
            </div>

        </div>                   

        <!-- End Re-exam working efford -->






    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_company").addClass("active open");
            $("#company_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");

        });

    </script>
@endsection
