<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Export</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr class="table-danger">
        <td>ID</td>
        <td>Name</td>
        <td>Description</td>
        <td>Requirement</td>
        <td>Location</td>
        <td>Salary</td>
        <td>Closed</td>
        <td>Category</td>
        <td>Company</td>
      </tr>
      </thead>
      <tbody>
        @foreach ($joblists as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->job_name }}</td>
            <td>{{ $data->job_description }}</td>
            <td>{{ $data->job_requirement }}</td>
            <td>{{ $data->job_location }}</td>
            <td>{{ $data->job_salary }}</td>
            <td>{{ $data->job_closed }}</td>
            <td>{{ $data->cat_name }}</td>
            <td>{{ $data->company_name }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>