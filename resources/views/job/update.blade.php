@extends('layouts.master')
@section('content')
<div class="card card-gray">
    <div class="card-block">
        <h5>Update Job
                <a href="{{ url('job/backendjobs') }}" class="btn btn-primary btn-sm btn-oval">
                    <i class="fa fa-reply"></i> Back</a>
        </h5>
        <hr>
        <form action="{{url('job/update')}}" method="POST" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="row">
                <div class="col-sm-9">
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>
                            {{session('success')}}
                        </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif

                    <div class="form-group row">
                        <label for="jobname" class="col-sm-4">Job Title 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="JobName" 
                                name='JobName' autofocus value="{{$job->job_name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jd" class="col-sm-4">Job Description 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" id="JobDescription" 
                                name='JobDescription' >{{$job->job_description}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jr" class="col-sm-4">Job Requirement 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" id="jr" 
                                name='JobRequirement'>{{$job->job_description}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="joblocation" class="col-sm-4">Job Location 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="joblocation" 
                                name='JobLocation' value="{{$job->job_location}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jobsalary" class="col-sm-4">Job Salary 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="jobsalary" 
                                name='JobSalary' value="{{$job->job_salary}}">
                        </div>
                    </div>  
                    <div class="form-group row">
                        <label for="jbclosedate" class="col-sm-4">Close Date 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="JobCloseDate" 
                                name='JobCloseDate' value="{{$job->job_closed}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="category" class="col-sm-4">Category Type 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                           <select name="Category" id="category" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($categories as $cat)
                                <option value="{{$cat->id}}" 
                                    {{$cat->id==$job->catid?'selected':''}} >{{$cat->cat_name}}</option>
                                @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="username" class="col-sm-4">Company 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                           <select name="Company" id="companyname" class="form-control" require>
                                <option value="">--Select--</option>
                                @foreach($companies as $com)
                                <option value="{{$com->id}}" 
                                    {{$com->id==$job->comid?'selected':''}} >{{$com->company_name}}</option>
                                @endforeach
                           </select>
                        </div>
                    </div>
                    <div style="display: none">
                        <input type="text" name="jobid" id="jobid" value="{{ $job->id}}">
                    </div>
                    <div style="display: none">
                        <input type="text" name="userid" id="userid" value="{{ Auth::user()->id }}">
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4"></label>
                        <div class="col-sm-8">
                            <button class="btn btn-primary btn-oval">
                                <i class="fa fa-save"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_job").addClass("active open");
            $("#job_collapse").addClass("collapse in");
            $("#menu_job_list").addClass("active");

        });

    </script>
@endsection