@extends('layouts.master')
@section('content')
<div class="card card-gray">
    <div class="card-block">
        <h5>Update Candidate
                <a href="{{url('candidate/backendcandidate')}}" class="btn btn-primary btn-sm btn-oval">
                    <i class="fa fa-reply"></i> Back</a>
        </h5>
        <hr>
        <form action="{{url('candidate/update')}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-sm-9">
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>
                            {{session('success')}}
                        </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                    <div class="form-group row">
                        <label for="canname" class="col-sm-4">Name 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="candname" 
                                name='candname' autofocus value="{{$candidate->can_name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="canemail" class="col-sm-4">Email
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="canemail" 
                                name='canemail' autofocus value="{{$candidate->can_email}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jr" class="col-sm-4">Phone 
                            <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="phone" 
                                name='phone' autofocus value="{{$candidate->phone_num}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="age" class="col-sm-4">Age <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="age" 
                                name='age' value="{{$candidate->age}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exp" class="col-sm-4">Experience</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="exp" 
                                name='exp' value="{{$candidate->work_experience}}">
                        </div>
                    </div>  
                    <div class="form-group row">
                        <label for="level" class="col-sm-4"> Level </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="level" 
                                name='level' value="{{$candidate->professional_level}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="edu" class="col-sm-4"> Education </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="edu" 
                                name='edu' value="{{$candidate->education}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lage" class="col-sm-4"> Language <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="lage" 
                                name='lang' value="{{$candidate->language}}">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="hbe" class="col-sm-4"> Hobbies</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="hbe" 
                                name='hobb' value="{{$candidate->hobbies}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="eval" class="col-sm-4"> Evaluation </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="eval" 
                                name='eval' value="{{$candidate->self_evaluate}}">
                        </div>
                    </div>

                    <div class="form-group row">
                                            <label for="photo" class="col-sm-4">Photo
                                            <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <img src="../../{{$candidate->image}}" alt="Photo {{$candidate->can_name}}" class="img-responsive" style="width: 15%"> <hr>
                                                <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">

                                            </div>
                                        </div>
                    
                    <div style="display: none">
                        <input type="text" name="id" id="id" value="{{ $candidate->id}}">
                    </div> 

                    <div class="form-group row">
                        <label class="col-sm-4"></label>
                        <div class="col-sm-8">
                            <button class="btn btn-primary btn-oval">
                                <i class="fa fa-save"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_candidate").addClass("active open");
            $("#candidate_collapse").addClass("collapse in");
            $("#menu_candidate_list").addClass("active");

        });

    </script>
@endsection