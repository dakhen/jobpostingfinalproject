<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Export</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr class="table-danger">
      <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Age</th>
        <th>Experience</th>
        <th>Level</th>
        <th>Education</th>                        
        <th>language</th>                       
      </tr>
      </thead>
      <tbody>
        @foreach ($candidateList as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->can_name }}</td>
            <td>{{ $data->can_email }}</td>
            <td>{{ $data->phone_num }}</td>
            <td>{{ $data->age }}</td>
            <td>{{ $data->work_experience }}</td>
            <td>{{ $data->professional_level }}</td>
            <td>{{ $data->education }}</td>
            <td>{{ $data->language }}</td>
            </tr>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>