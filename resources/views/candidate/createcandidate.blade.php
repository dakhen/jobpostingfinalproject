@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-gray">
                    <div class="card-block">
                        <h5 style="text-align: center;"><b>Create Candidate Profile</b></h5>
                        <hr>
                        <form action="{{ url('/candidate/saveCandidate') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-9">
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <p>
                                                    {{ session('success') }}
                                                </p>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4">Name
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="candname" name='candname' 
                                                    autofocus value="{{ old('cand_name') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-sm-4">Email
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="canemail" name='canemail' 
                                                    autofocus value="{{ old('canemail') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="col-sm-4">Phone
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="phone" name='phone'
                                                     value="{{ old('phone') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="age" class="col-sm-4">Age
                                                <span class="text-danger">*</span></label>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="number" class="form-control" id="age" name='age'
                                                     value="{{ old('age') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exp" class="col-sm-4">Experience</label>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="number" class="form-control" id="experience" name='experience'
                                                     value="{{ old('experience') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="level" class="col-sm-4">Level</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="level" name='level'
                                                     value="{{ old('level') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="edu" class="col-sm-4">Education
                                                <span class="text-danger">*</span></label>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="edu" name='edu'
                                                     value="{{ old('edu') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="lang" class="col-sm-4">Language
                                                <span class="text-danger">*</span></label>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="lang" name='lang'
                                                     value="{{ old('lang') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="hobb" class="col-sm-4">Hobbie</label>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="hobb" name='hobb'
                                                     value="{{ old('hobb') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="eval" class="col-sm-4">Evaluation</label>
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="eval" name='eval'
                                                     value="{{ old('eval') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="photo" class="col-sm-4">Photo
                                            <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" >
                                            </div>
                                        </div>

                                        <div style="display: none">
                                            <input type="text" name="userid" id="userid" value="{{ Auth::user()->id }}">
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button class="btn btn-primary btn-oval">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                                <a href="{{ url('/candidate/backendcandidate') }}" class="btn btn-primary btn-oval">
                                                    <i class="fa fa-reply"></i> Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_candidate").addClass("active open");
            $("#candidate_collapse").addClass("collapse in");
            $("#menu_candidate").addClass("active");

        });

        function loadPhoto(e) {
            var img = document.getElementById('img');
            img.src = URL.createObjectURL(e.target.files[0]);
        }

    </script>
@endsection
