@extends('layouts.master')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <button class="btn btn-success pull-right dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download" ></i> Download
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        &nbsp;<a href="{{url('candidate/exportexcel')}}" style="text-decoration: none;"><i class="fa fa-file-excel-o" ></i> <b>Excel</b> file</a><br>
                        &nbsp;<a href="{{url('candidate/exportword')}}" style="text-decoration: none;"><i class="fa fa-file-word-o" ></i> <b>Word</b> file</a><br>
                        &nbsp;<a href="{{url('candidate/exportpdf')}}" style="text-decoration: none;"><i class="fa fa-file-pdf-o" ></i> <b>Pdf</b> file</a><br>
                    </div><br><br>

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Experience</th>
                                <th>Level</th>
                                <th>Education</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $pagex = @$_GET['page'];
                                if (!$pagex) {
                                $pagex = 1;
                                }
                                $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($candidateList as $candidate)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>
                                        <img src="../../{{$candidate->image}}" alt="Photo {{$candidate->can_name}}" class="img-thumbnail mr-2" style="max-height:50px; max-width:auto;">
                                    </td>
                                    <td>{{ $candidate->can_name }}</td>
                                    <td>{{ $candidate->can_email }}</td>
                                    <td>{{ $candidate->phone_num }}</td>
                                    <td>{{ $candidate->work_experience }}</td>
                                    <td>{{ $candidate->professional_level }}</td>
                                    <td>{{ $candidate->education }}</td>
                                    
                                    <td>
                                        <a href="{{ url('candidate/detailCandidate/' . $candidate->id) }}"
                                            class="text-primary" title="Can Detail">
                                            <span><i class="fa fa-info-circle"></i> </span>
                                        </a>&nbsp;
                                        <a href="{{ url('candidate/editCandidate/' . $candidate->id) }}"
                                            class="text-success" title="Update Candidate">
                                            <i class="fa fa-edit"></i>
                                        </a>&nbsp;
                                        <a href="{{ url('candidate/deleteCandidate?id=' . $candidate->id) }}"
                                            class="text-danger" title="Delete candidate"
                                            onclick="return confirm('Are you sure to delete?')">
                                            <i class="fa fa-trash"></i>
                                        </a>

                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>

        </div>
    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_candidate").addClass("active open");
            $("#candidate_collapse").addClass("collapse in");
            $("#menu_candidate").addClass("active");

        });

    </script>
@endsection
