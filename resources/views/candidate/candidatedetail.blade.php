@extends('layouts.master')
@section('content')
<section id="featured-services" class="height-menu"></section>
<section class="margin-section-content">
    <div class="container">
    <!-- <br> -->
    <div class="row">
        <div class="col-md-4">
            <h6><b>{{$candidate->can_name}}</b></h6>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
        <div class="card card-border-none">
            <!-- <div class="card-header">Candidate Profile</div> -->
            <div class="card-body">
                    <div class="row">
                        <img src="../../{{$candidate->image}}" alt="photo {{$candidate->can_name}}" style="width: 20%"><br>
                <div class="col-md-3">
                    <span>Candidate Name</span>
                </div>
                <div class="col-md-5 text-left text-primary">
                    <span id="">{{$candidate->can_name}}</span> 
                </div>
                <div class="col-md-4"></div>
            </div><br>
            <div class="row">
                <div class="col-md-3">
                    <span>Candidate Email</span>
                </div>
                <div class="col-md-5 text-left">
                    <span id="">{{$candidate->can_email}}</span> 
                </div>
                <div class="col-md-4"></div>
            </div><br>
            <div class="row">
                <div class="col-md-3">
                    <span>Contact</span>
                </div>
                <div class="col-md-5 text-left">
                    <span id="">{{$candidate->phone_num}}</span>
                </div>
                <div class="col-md-4"></div>
            </div><br>
            <div class="row">
                <div class="col-md-3">
                    <span>Experience</span>
                </div>
                <div class="col-md-5 text-left">
                    <span id="">{{$candidate->work_experience}}</span>
                </div>
                <div class="col-md-4"></div>
            </div><br>
            <div class="row">
                <div class="col-md-3">
                    <span>Level</span>
                </div>
                <div class="col-md-5 text-left">
                    <span id="">{{$candidate->professional_level}}</span>
                </div>
                <div class="col-md-4"></div>
            </div><br>
            <div class="row">
                <div class="col-md-3">
                    <span>Education</span>
                </div>
                <div class="col-md-5 text-left">
                    <span id="">{{$candidate->education}}</span>
                </div>
                <div class="col-md-4"></div>
            </div><br>
            <div class="row">
                <div class="col-md-3">
                    <span>Created Date</span>
                </div>
                <div class="col-md-5 text-left">
                    <span id="">{{$candidate->created_at}}</span>
                </div>
                <div class="col-md-4"></div>
            </div><br>
            </div>
        </div>
        </div>
    </div>
    
    <br>
    

    </div>
</section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_candidate").addClass("active open");
            $("#candidate_collapse").addClass("collapse in");
            $("#menu_candidate").addClass("active");

        });

    </script>
@endsection