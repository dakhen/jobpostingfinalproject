@extends('layouts.master')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-success pull-right dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download" ></i> Download
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        &nbsp;<a href="{{url('category/exportexcel')}}" style="text-decoration: none;"><i class="fa fa-file-excel-o" ></i> <b>Excel</b> file</a><br>
                        &nbsp;<a href="{{url('category/exportword')}}" style="text-decoration: none;"><i class="fa fa-file-word-o" ></i> <b>Word</b> file</a><br>
                        &nbsp;<a href="{{url('category/exportpdf')}}" style="text-decoration: none;"><i class="fa fa-file-pdf-o" ></i> <b>Pdf</b> file</a><br>
                    </div><br><br>

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr style="text-align: center">
                                <th scope="col">ID</th>
                                <th scope="col">Job Categories</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if (!$pagex) {
                            $pagex = 1;
                            }
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($categories as $category)
                                <tr style="text-align: center">
                                    <td>{{ $i++ }}</td>
                                    <td style="text-align: left">{{$category->cat_name}}</td>
                                    <td>{{$category->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        <div class="row" style="margin-left: 5px;">
                                            {{-- <a href="{{ url('backend/user/category/categories/' . $category->id . '/edit') }}"
                                                class="text-success" title="Update Category">
                                                <i class="fa fa-edit"></i>
                                            </a> --}}
                                            <form style="margin-top: 4px">
                                                <a href="{{ url('backend/user/category/categories/' . $category->id . '/edit') }}">
                                                  <i style="color: rgb(20, 175, 20)" class="fa fa-edit"></i>
                                                </a>
                                              </form><span>&nbsp;&nbsp;&nbsp;</span>
                                            <form action="{{url('backend/user/category/categories', [$category->id])}}" method="post">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button style="border: 0px" type="button" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                                                <i style="color: red; " class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>

        </div>
    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");
            $("#menu_cat").addClass("active open");
            $("#cat_collapse").addClass("collapse in");
            $("#menu_cat_list").addClass("active");

        });

    </script>
@endsection
