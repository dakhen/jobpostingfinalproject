<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Export</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr class="table-danger">
      <th>ID</th>
        <th>Category name</th>
        <th>Created at</th>
        <th>Updated At</th>
      </tr>
      </thead>
      <tbody>
        @foreach ($categories as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->cat_name }}</td>
            <td>{{ $data->created_at }}</td>
            <td>{{ $data->updated_at }}</td>
            </tr>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
