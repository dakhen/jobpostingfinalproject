@extends('layouts.front')
@section('title', 'List of Company')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <!-- <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr style="text-align: center">
                                <th scope="col">ID</th>
                                <th scope="col">Job Categories</th>
                                {{-- <th scope="col">Created At</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if (!$pagex) {
                            $pagex = 1;
                            }
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($categories as $category)
                                <tr style="text-align: center">
                                    <td>{{ $i++ }}</td>
                                    <td style="text-align: left">{{$category->cat_name}}</td>
                                    {{-- <td>{{$category->created_at->toFormattedDateString()}}</td> --}}
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>

        </div> -->

        <div class="container-fluid">
            <div class="panel panel-default">
                <div class="panel-heading panel-info">
                    <h3 class="panel-title "><b>Search Category</b></h3>
                </div>
                <div class="panel-body">
                <form action="{{ url('/category/searchCategory') }}"  method="GET">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="inputState">Keyword</label>
                            <input type="text" class="form-control" placeholder="keyword" id="keyword" name='keyword' />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputState">Category Name</label>
                            <select id="inputState" class="form-control" name="category">
                                <option value="">--Select--<option>
                                @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->cat_name}}<option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group col-md-3"><br>
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                       
                    </div>

                </form>
                    <br>
                    <div class="jobs">
                        <div class="row" style="margin-right: 0px;margin-left: 0px;">
                            @foreach ($categorylist as $category)
                                <div class="col-md-3 list-container">
                                    <a href="#" class="list-group-item"><span class="cname">{{$category->cat_name}}</span>
                                        <span class="badge" style="text-align: right">1</span>
                                    </a>
                                </div>
                            @endforeach
                        </div>

                    </div>
                        
                    </div><br>

                </div>
            </div>

        </div>   
    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");
            $("#menu_cat").addClass("active open");
            $("#cat_collapse").addClass("collapse in");
            $("#menu_cat_list").addClass("active");

        });

    </script>
@endsection
