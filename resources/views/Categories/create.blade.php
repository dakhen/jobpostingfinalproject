@extends('layouts.master')
@section('content')

<h1>
    Add Category
</h1>

<form action="/backend/user/category/categories" method="post">
    {{ csrf_field() }}
     <div class="form-group">
       <label for="title">Category Name</label>
       <input type="text" class="form-control" id="cat_name"  name="cat_name">
     </div>
     @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
     <button type="submit" class="btn btn-primary">Submit</button>
   </form>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_cat").addClass("active open");
            $("#cat_collapse").addClass("collapse in");
            $("#menu_cats").addClass("active");

        });

    </script>
@endsection

