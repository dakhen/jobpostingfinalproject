@extends('layouts.master')
@section('content')

    <hr> {{-- <form action="task/{{ $task->id }}" method="post"> --}}
     <form action="{{url('backend/user/category/categories', [$category->id])}}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group">
        <label for="title">Category Name</label>
        <input type="text" value="{{$category->cat_name}}" class="form-control" id="cat_name"  name="cat_name" >
      </div>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    @endsection
