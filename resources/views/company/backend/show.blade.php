@extends('layouts.master')
@section('title', 'List of Company')
@section('content')
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
        <br>
        <div class="row">
            <h5>Company Detail
                <a href="{{ route('company.index') }}" class="btn btn-primary btn-sm btn-oval">
                    <i class="fa fa-reply"></i> Back</a>
            </h5>
            <hr>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
            <div class="card card-border-none">
                {{-- <div class="card-header">Company Detail</div> --}}
                <div class="card-body">
                    <div class="row">
                <div class="col-md-3">
                    <span>Company Name</span>
                </div>
                <div class="col-md-5 text-left">
                    <span id="">{{ $companies->company_name }}</span>
                </div>
                <div class="col-md-4"></div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <span>Company Mission</span>
                    </div>
                    <div class="col-md-5 text-left">
                        <span id="">{{ $companies->company_mission }}</span>
                    </div>
                    <div class="col-md-4"></div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <span>Company Vision</span>
                    </div>
                    <div class="col-md-5 text-left">
                        <span id="">{{ $companies->company_vision }}</span>
                    </div>
                    <div class="col-md-4"></div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <span>Company Type</span>
                    </div>
                    <div class="col-md-5 text-left">
                        <span id="">{{ $companies->company_type }}</span>
                    </div>
                    <div class="col-md-4"></div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <span>Employee</span>
                    </div>
                    <div class="col-md-5 text-left">
                        <span id="">{{ $companies->company_emp_num }}</span>
                    </div>
                    <div class="col-md-4"></div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <span>Company Website</span>
                    </div>
                    <div class="col-md-5 text-left">
                        <span id="">{{ $companies->company_website }}</span>
                    </div>
                    <div class="col-md-4"></div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <span>Company Contact</span>
                    </div>
                    <div class="col-md-5 text-left">
                        <span id="">{{ $companies->company_contact }}</span>
                    </div>
                    <div class="col-md-4"></div>
                </div><br>
                <div class="row">
                    <div class="col-md-3">
                        <span>User</span>
                    </div>
                    <div class="col-md-5 text-left">
                        <span id="">{{ Auth::user()->name }}</span>
                    </div>
                    <div class="col-md-4"></div>
                </div><br>
                </div>
            </div>
            </div>
        </div>

        </div>
    </section><!-- #featured-services -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_company").addClass("active open");
            $("#company_collapse").addClass("collapse in");
            $("#menu_company_list").addClass("active");

        });

    </script>
@endsection