@extends('layouts.master')
@section('title', 'Update Company')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-gray">
                    <div class="card-block">
                        <h5 style="text-align: center;"><b>Update Company</b></h5>
                        <hr>
                        <form action="{{ route('company.update', $companies->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-9">
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <p>
                                                    {{ session('success') }}
                                                </p>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <label for="company_name" class="col-sm-4">Company Name
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input value="{{ $companies->company_name }}" type="text" class="form-control" id="company_name" name='company_name'
                                                    autofocus value="{{ old('company_name') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="jd" class="col-sm-4">Company Mission
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input value="{{ $companies->company_mission }}" type="text" class="form-control" id="company_mission" name='company_mission'
                                                    value="{{ old('company_mission') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="company_vision" class="col-sm-4">Company Vision
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input value="{{ $companies->company_vision }}" type="text" class="form-control" id="company_vision" name='company_vision'
                                                    value="{{ old('company_vision') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="company_type" class="col-sm-4">Company Type
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input value="{{ $companies->company_type }}" type="text" class="form-control" id="company_type" name='company_type'
                                                 value="{{ old('company_type') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="employee" class="col-sm-4">Employee
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input value="{{ $companies->company_emp_num }}" type="text" class="form-control" id="employee" name='employee'
                                                 value="{{ old('employee') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="company_website" class="col-sm-4">Company Website
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input value="{{ $companies->company_website }}" type="text" class="form-control" id="company_website" name='company_website'
                                                 value="{{ old('company_website') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="company_contact" class="col-sm-4">Company Contact
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input value="{{ $companies->company_contact }}" type="text" class="form-control" id="company_contact" name='company_contact'
                                                 value="{{ old('company_contact') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="photo" class="col-sm-4">Photo
                                            <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <img src="../{{$companies->image}}" alt="Photo {{$companies->company_name}}" class="img-responsive" style="width: 50%"> <hr>
                                                <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">

                                            </div>
                                        </div>
                                        

                                        <div class="form-group row">
                                            <label class="col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button class="btn btn-primary btn-oval">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                                <a href="{{ route('company.index') }}" class="btn btn-primary btn-oval">
                                                    <i class="fa fa-reply"></i> Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_company").addClass("active open");
            $("#company_collapse").addClass("collapse in");
            $("#menu_company_list").addClass("active");

        });

    </script>
@endsection