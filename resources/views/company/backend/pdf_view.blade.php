<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Export</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr class="table-danger">
        <td>ID</td>
        <td>Company Name</td>
        <td>Company Mission</td>
        <td>Company Vision</td>
        <td>Company Type</td>
        <td>Employee</td>
        <td>Company Website</td>
        <td>Company Contact</td>
      </tr>
      </thead>
      <tbody>
        @foreach ($companies as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->company_name }}</td>
            <td>{{ $data->company_mission }}</td>
            <td>{{ $data->company_vision }}</td>
            <td>{{ $data->company_type }}</td>
            <td>{{ $data->employee }}</td>
            <td>{{ $data->company_website }}</td>
            <td>{{ $data->company_contact }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>