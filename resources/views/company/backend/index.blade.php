@extends('layouts.master')
@section('title', 'List of Company')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <button class="btn btn-success pull-right dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download" ></i> Download
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        &nbsp;<a href="{{url('company/exportexcel')}}" style="text-decoration: none;"><i class="fa fa-file-excel-o" ></i> <b>Excel</b> file</a><br>
                        &nbsp;<a href="{{url('company/exportword')}}" style="text-decoration: none;"><i class="fa fa-file-word-o" ></i> <b>Word</b> file</a><br>
                        &nbsp;<a href="{{url('company/exportpdf')}}" style="text-decoration: none;"><i class="fa fa-file-pdf-o" ></i> <b>Pdf</b> file</a><br>
                    </div><br><br>

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                              <td >&numero;</td>
                              <th scope="col">{{ __('Name') }}</th>
                              <th scope="col">{{ __('Mission') }}</th>
                              <th scope="col">{{ __('Vision') }}</th>
                              <th scope="col">{{ __('Type') }}</th>
                              <th scope="col">{{ __('Eployee') }}</th>
                              <th scope="col">{{ __('Website') }}</th>
                              <th scope="col">{{ __('Contact') }}</th>
                              <th scope="col">{{ __('User') }}</th>
                              <th scope="col">{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if (!$pagex) {
                            $pagex = 1;
                            }
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($companies as $company)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td style="word-break:break-all">{{ $company->company_name }}</td>
                                    <td style="word-break:break-all">{{ $company->company_mission }}</td>
                                    <td style="word-break:break-all">{{ $company->company_vision }}</td>
                                    <td style="word-break:break-all">{{ $company->company_type }}</td>
                                    <td style="word-break:break-all">{{ $company->company_emp_num }}</td>
                                    <td style="word-break:break-all">{{ $company->company_website }}</td>
                                    <td style="word-break:break-all">{{ $company->company_contact }}</td>
                                    {{-- <td>{{ Auth::user()->name }}</td> --}}
                                    <td>{{ $company->user->name }}</td>
                                    <td>
                                        <a href="{{ route('company.show', $company->id) }}"
                                            class="text-primary" title="Company Detail">
                                            <span><i class="fa fa-info-circle"></i> </span>
                                        </a>
                                        <a href="{{ route('company.edit', $company->id) }}"
                                            class="text-success" title="Update Company">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{ route('company.destroy', $company->id) }}"
                                            class="text-danger" title="Delete Company"
                                            onclick="return confirm('Are you sure to delete?')">

                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>
        </div>
  </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_company").addClass("active open");
            $("#company_collapse").addClass("collapse in");
            $("#menu_company_list").addClass("active");

        });
    </script>
@endsection