@extends('layouts.front')
@section('title', 'List of Company')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container-fluid">
            <div class="panel panel-default">
                <div class="panel-heading panel-info">
                    <h3 class="panel-title "><b>Search Company</b></h3>
                </div>
                <div class="panel-body">
                <form action="{{ url('/company/searchCompany') }}"  method="GET">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="inputState">Keyword</label>
                            <input type="text" class="form-control" placeholder="keyword" id="keyword" name='keyword' />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputState">Company Name</label>
                            <select id="inputState" class="form-control" name="company">
                                <option value="">--Select--<option>
                                @foreach ($companies as $company)
                                    <option value="{{$company->company_name}}">{{$company->company_name}}<option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group col-md-3"><br>
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                       
                    </div>

                </form>
                    <br>
                    @foreach ($companylist as $company)
                    <div class="jobs">
                        <div class="row job_listing" style="margin-right: 0px;margin-left: 0px;">
                            <div class="col-md-3 company-info">
                            <div class="company-name mainf">
                                <a href="#">
                                    <img src="../{{$company->image}}" alt="Photo {{$company->company_name}}" class="img-responsive company-logo"><br><br>
                                    <span><a href="#" class="ComDescription">{{ $company->company_name }}</a></span>
                                </a>
                            </div>        

                            </div>

                            <div class="col-md-9 job-info">
                            <div class="job-details">
                                    <div class="job_detail_page">
                                        <div class="row">

                                            <div class="job-salary col-md-4">
                                                <label for="">Company Name : </label>
                                                <span>{{ $company->company_name }}</span> 
                                            </div>

                                            <div class="job-salary col-md-4">
                                                <label for="">Company Type : </label>
                                                <span>{{ $company->company_type }}</span> 

                                            </div>

                                            <div class="job-salary col-md-4">
                                                <label for="">Website : </label>
                                                <span>{{ $company->company_website }}</span>    

                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="job-salary col-md-4">
                                                <label for="">Vision : </label>
                                                <span>{{ $company->company_vision }}</span> 

                                            </div>
                                            <div class="job-salary col-md-4">
                                                <label for="">Mission : </label>
                                                <span>{{ $company->company_mission }}</span>
                                                
                                            </div>

                                            <div class="job-salary col-md-4">
                                                <label for="">Contact : </label>
                                                <span>{{ $company->company_contact }}</span>  

                                            </div>

                                        </div>
                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div><br>
                    @endforeach

                </div>
            </div>

        </div>                   

        <!-- End Re-exam working efford -->
  </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_company").addClass("active open");
            $("#company_collapse").addClass("collapse in");
            $("#menu_company_list").addClass("active");

        });
    </script>
@endsection