<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Export</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr class="table-danger">
        <td>ID</td>
        <td>Role</td>
        <td>Created At</td>
        <td>Updated At</td>
      </tr>
      </thead>
      <tbody>
        @foreach ($rolelists as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->role_name }}</td>
            <td>{{ $data->created_at }}</td>
            <td>{{ $data->updated_at }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>