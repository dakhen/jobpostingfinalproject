@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-gray">
                <div class="card-block">
                    <h5>Update Role
                        <a href="{{url('role/rolelist')}}" class="btn btn-primary btn-sm btn-oval">
                            <i class="fa fa-reply"></i> Back</a>
                    </h5>
                    <hr>
                    <form action="{{url('role/updaterole')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-9">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <p>
                                        {{session('success')}}
                                    </p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                @endif
                                <div class="form-group row">
                                    <label for="rolename" class="col-sm-4">Rolename 
                                        <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="rolename" 
                                            name='rolename' autofocus value="{{$role->role_name}}">
                                    </div>
                                </div>
                               

                                <div style="display: none">
                                    <input type="text" name="id" id="id" value="{{ $role->id}}">
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4"></label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-primary btn-oval">
                                            <i class="fa fa-save"></i> Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_security").addClass("active open");
            $("#security_collapse").addClass("collapse in");
            $("#men_role").addClass("active");
        });

    </script>
@endsection