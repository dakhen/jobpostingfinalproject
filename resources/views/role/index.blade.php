@extends('layouts.master')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary btn-oval">
                    <a href="{{url('role/createrole')}}" class="text-white" style="text-decoration: none;">
                        <i class="fa fa-plus" ></i> Create Role
                    </a>
                    </button>

                    <button class="btn btn-success pull-right dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download" ></i> Download
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        &nbsp;<a href="{{url('role/exportexcel')}}" style="text-decoration: none;"><i class="fa fa-file-excel-o" ></i> <b>Excel</b> file</a><br>
                        &nbsp;<a href="{{url('role/exportword')}}" style="text-decoration: none;"><i class="fa fa-file-word-o" ></i> <b>Word</b> file</a><br>
                        &nbsp;<a href="{{url('role/exportpdf')}}" style="text-decoration: none;"><i class="fa fa-file-pdf-o" ></i> <b>Pdf</b> file</a><br>
                    </div><br><br>

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Role Name</th>
                                <th scope="col">Create Date</th>
                                <th scope="col">Update Date</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if (!$pagex) {
                            $pagex = 1;
                            }
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($rolelists as $role)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$role->role_name}}</td>
                                    <td>{{$role->created_at}}</td>
                                    <td>{{$role->updated_at}}</td>

                                    <td>
                                        <a href="{{url('role/editrole/'.$role->id)}}" class="text-success" title="Update User">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{url('role/deleterole?id='.$role->id)}}" class="text-danger" title="Delete User"
                                        onclick="return confirm('Are you sure to delete?')">
                                         
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>

        </div>
    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_security").addClass("active open");
            $("#security_collapse").addClass("collapse in");
            $("#men_role").addClass("active");

        });

    </script>
@endsection
