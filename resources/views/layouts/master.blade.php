<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>My Admin</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="{{asset('admin/apple-touch-icon.png')}}">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="{{asset('admin/css/vendor.css')}}">
        <link rel="stylesheet" href="{{asset('admin/css/app-green.css')}}">

        {{-- FontAwsome --}}
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--Bootsrap 4 CDN-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>
            <div class="main-wrapper">
                    <div class="app" id="app">
                        <header class="header">
                            <div class="header-block header-block-collapse d-lg-none d-xl-none">
                                <button class="collapse-btn" id="sidebar-collapse-btn">
                                    <i class="fa fa-bars"></i>
                                </button>
                            </div>
                            <div class="header-block header-block-search">
                                <form role="search">
                                    <div class="input-container">
                                        <i class="fa fa-search"></i>
                                        <input type="search" placeholder="Search">
                                        <div class="underline"></div>
                                    </div>
                                </form>
                            </div>
                            <div class="header-block header-block-buttons">
                               {{date('Y-m-d h:i:s a')}}
                            </div>
                            <div class="header-block header-block-nav">
                                <ul class="nav-profile">
                                    <li class="profile dropdown">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                            {{-- for user image --}}
                                            {{-- <div class="img" style="background-image: url('https://avatars3.githubusercontent.com/u/3959008?v=3&s=40')">
                                            </div> --}}
                                            <span class="name"> {{ Auth::user()->name }} </span>
                                        </a>
                                        <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();">
                                             {{ __('Logout') }}
                                         </a>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                             @csrf
                                         </form>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </header>
                        <aside class="sidebar">
                            <div class="sidebar-container">
                                <div class="sidebar-header">
                                    <div class="brand">
                                        <div class="logo">
                                            <span class="l l1"></span>
                                            <span class="l l2"></span>
                                            <span class="l l3"></span>
                                            <span class="l l4"></span>
                                            <span class="l l5"></span>
                                        </div> My Admin
                                    </div>
                                </div>
                                <nav class="menu">
                                    <ul class="sidebar-menu metismenu" id="sidebar-menu">
                                        <li class="active">
                                            <a href="{{url('home')}}">
                                                <i class="fa fa-home"></i> Dashboard </a>
                                        </li>
                                        @if (Auth::user()->role_id == 1)
                                        <li id='menu_security'>
                                            <a href="">
                                                <i class="fa fa-users"></i> User Management <i class="fa arrow"></i>
                                            </a>
                                            <ul class="sidebar-nav" id="security_collapse">
                                                <li id="menu_user">
                                                    <a href="{{url('user/userlist')}}">
                                                       <i class="fa fa-arrow-right"></i> Users </a>
                                                </li>
                                                <li id="men_role">
                                                    <a href="{{url('role/rolelist')}}">
                                                        <i class="fa fa-arrow-right"></i> Roles </a>
                                                </li>
                                            </ul>
                                        </li>
                                        @endif
                                        <li id='menu_company'>
                                            <a href="">
                                                <i class="fa fa-building"></i> Company Management <i class="fa arrow"></i>
                                            </a>
                                            <ul class="sidebar-nav" id="company_collapse">
                                                @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                                <li id="menu_companies">
                                                    <a href="{{url('company/create')}}"> 
                                                       <i class="fa fa-arrow-right"></i> Create New Company </a>
                                                </li>
                                                @endif
                                                <li id="menu_company_list">
                                                    <a href="{{url('/company')}}"> 
                                                        <i class="fa fa-arrow-right"></i> All Companies </a>
                                                </li>
                                            </ul>
                                        </li>
                                        {{-- <li id='menu_category'>
                                            <a href="">
                                                <i class="fa fa-table"></i> Category Management<i class="fa arrow"></i>
                                            </a>
                                            <ul class="sidebar-nav" id="cats_collapse">
                                                <li id="menu_cats">
                                                    <a href="{{url('job/createjob')}}">
                                                       <i class="fa fa-arrow-right"></i> Post New Category </a>
                                                </li>
                                                <li id="menu_cats_list">
                                                    <a href="{{url('job/backendjobs')}}">
                                                        <i class="fa fa-arrow-right"></i> All Categories </a>
                                                </li>
                                            </ul>
                                        </li> --}}
                                        <li id='menu_job'>
                                            <a href="">
                                                <i class="fa fa-briefcase"></i> Job Management<i class="fa arrow"></i>
                                            </a>
                                            <ul class="sidebar-nav" id="job_collapse">
                                                @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                                <li id="menu_jobs">
                                                    <a href="{{url('job/createjob')}}">
                                                       <i class="fa fa-arrow-right"></i> Post New Job </a>
                                                </li>
                                                @endif
                                                <li id="menu_job_list">
                                                    <a href="{{url('job/backendjobs')}}">
                                                        <i class="fa fa-arrow-right"></i> All Jobs </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li id='menu_candidate'>
                                            <a href="">
                                                <i class="fa fa-user"></i> Candidate Management<i class="fa arrow"></i>
                                            </a>
                                            <ul class="sidebar-nav" id="candidate_collapse">
                                                <li id="menu_cands">
                                                    <a href="{{url('candidate/createcandidate')}}">
                                                       <i class="fa fa-arrow-right"></i> Create Candidate Profile </a>
                                                </li>
                                                @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                                <li id="menu_cand_list">
                                                    <a href="{{url('candidate/backendcandidate')}}">
                                                        <i class="fa fa-arrow-right"></i> Candidate List </a>
                                                </li>
                                                @endif
                                            </ul>
                                        </li>
                                        @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                        <li id='menu_cat'>
                                            <a href="">
                                                <i class="fa fa-table" aria-hidden="true"></i> Category <i class="fa arrow"></i>
                                            </a>
                                            <ul class="sidebar-nav" id="cat_collapse">

                                                <li id="menu_cats">
                                                    <a href="{{url('backend/user/category/categories/create')}}">
                                                        <i class="fa fa-arrow-right"></i> Post New Category </a>
                                                </li>
                                                <li id="menu_cat_list">
                                                    <a href="{{url('backend/user/category/categories')}}">
                                                        <i class="fa fa-arrow-right"></i> Category List </a>
                                                </li>
                                            </ul>
                                        </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                            <footer class="sidebar-footer">

                            </footer>
                        </aside>
                        <div class="sidebar-overlay" id="sidebar-overlay"></div>
                        <div class="sidebar-mobile-menu-handle" id="sidebar-mobile-menu-handle"></div>
                        <div class="mobile-menu-handle"></div>
                        <article class="content dashboard-page">

                            @yield('content')
                        </article>
                        <footer class="footer">
                            <div class="footer-block buttons">
                            </div>
                            <div class="footer-block author">

                         <ul>
                                    <li> created by <a href="#">Admin</a>
                                    </li>

                                </ul>
                            </div>
                        </footer>

                    </div>
                </div>


        <script src="{{asset('admin/js/vendor.js')}}"></script>
        <script src="{{asset('admin/js/app.js')}}"></script>
        @yield('script')
    </body>
</html>
