@extends('layouts.master')
@section('content')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>
                {{ session('success') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>
                {{ session('error') }}
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <section id="featured-services" class="height-menu"></section>
    <section class="margin-section-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary btn-oval">
                    <a href="{{url('user/creatuser')}}" class="text-white" style="text-decoration: none;">
                        <i class="fa fa-plus" ></i> Create User
                    </a>
                    </button>
                    
                    <button class="btn btn-success pull-right dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download" ></i> Download
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        &nbsp;<a href="{{url('user/exportexcel')}}" style="text-decoration: none;"><i class="fa fa-file-excel-o" ></i> <b>Excel</b> file</a><br>
                        &nbsp;<a href="{{url('user/exportword')}}" style="text-decoration: none;"><i class="fa fa-file-word-o" ></i> <b>Word</b> file</a><br>
                        &nbsp;<a href="{{url('user/exportpdf')}}" style="text-decoration: none;"><i class="fa fa-file-pdf-o" ></i> <b>Pdf</b> file</a><br>
                    </div><br><br>

                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if (!$pagex) {
                            $pagex = 1;
                            }
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($userlists as $user)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        <a href="{{url('user/userdetail/'.$user->id)}}" class="text-primary" title="User Detail">
                                            <span><i class="fa fa-info-circle" aria-hidden="true"></i> </span>
                                        </a>
                                        <a href="{{url('user/edit/'.$user->id)}}" class="text-success" title="Update User">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{url('user/deleteUser?id='.$user->id)}}" class="text-danger" title="Delete User"
                                        onclick="return confirm('Are you sure to delete?')">
                                         
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="row">
            </div>

        </div>
    </section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_security").addClass("active open");
            $("#security_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
        });

    </script>
@endsection
