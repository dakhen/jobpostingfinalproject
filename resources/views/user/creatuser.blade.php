@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-gray">
                    <div class="card-block">
                        <h5 style="text-align: center;"><b>Add New User</b></h5>
                        <hr>
                        <form action="{{ url('/user/saveuser') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-9">
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <p>
                                                    {{ session('success') }}
                                                </p>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif


                                        <div class="form-group row">
                                            <label for="username" class="col-sm-4">Username
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="username" name='username'
                                                    autofocus value="{{ old('username') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-sm-4">Email
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                            <input type="email" class="form-control" id="email" name='email'
                                                 value="{{ old('email') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="pass" class="col-sm-4">Password
                                                <span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="password" class="form-control" id="pass" name='pass'
                                                     value="{{ old('pass') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-primary btn-oval">
                                                    <i class="fa fa-save"></i> Add
                                                </button>
                                                <a href="{{ url('/user/userlist') }}" class="btn btn-primary btn-oval">
                                                    <i class="fa fa-reply"></i> Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_security").addClass("active open");
            $("#security_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");

        });

        function loadPhoto(e) {
            var img = document.getElementById('img');
            img.src = URL.createObjectURL(e.target.files[0]);
        }

    </script>
@endsection
