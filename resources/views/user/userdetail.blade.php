@extends('layouts.master')
@section('content')
<section id="featured-services" class="height-menu"></section>
<section class="margin-section-content">
    <div class="container">
        <h3>User Detail</h3><br>
        <div class="row">
            <div class="col-md-6">
                <div class="">
                    <div class=""><b>Username:</b> {{$user->name}}</div>
                    <div class=""><b>Email:</b> {{$user->email}}</div>
                </div>
            </div>
        </div>
    </div>
</section><!-- #featured-services -->


@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#sidebar-menu li ").removeClass("active open");
            $("#sidebar-menu li ul li").removeClass("active");

            $("#menu_security").addClass("active open");
            $("#security_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
        });

    </script>
@endsection