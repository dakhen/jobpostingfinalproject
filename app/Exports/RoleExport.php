<?php

namespace App\Exports;

use App\Model\Role;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RoleExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Role::all();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Role',
            'Created At',
            'Updated At',
        ];

    }
}
