<?php

namespace App\Exports;

use App\Categories;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class CategoryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = Categories::all();
        return $data;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Category Name',
            'Created At',
            'Updated At',
        ];

    }
}
