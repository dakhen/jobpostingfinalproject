<?php

namespace App\Exports;

use App\Model\Job;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class JobExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->select('jobs.id','job_name','job_description','job_requirement','job_location','job_salary','job_closed','cat_name', 'company_name')
        ->get();
        return $data;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Job Name',
            'Job Description',
            'Job Requirement',
            'Job Location',
            'Job Salary',
            'Job Closed',
            'Category',
            'Company',
        ];

    }
}
