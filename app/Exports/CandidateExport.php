<?php

namespace App\Exports;

use App\Model\Candidate;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class CandidateExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('candidates')
        ->select('id','can_name','can_email','phone_num','age','work_experience','professional_level','education', 'language')
        ->get();
        return $data;
    }

    public function headings(): array
    {
        return [

            'ID',
            'Name',
            'Email',
            'Phone',
            'Age',
            'Experience',
            'Level',
            'Education',
            'Language',
        ];

    }
}
