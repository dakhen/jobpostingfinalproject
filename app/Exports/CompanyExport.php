<?php

namespace App\Exports;

use App\Model\Company;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class CompanyExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('companies')
        ->select('ID','company_name','company_mission','company_vision','company_type','company_emp_num','company_website','company_contact')
        ->get();
        return $data;
    }

    public function headings(): array
    {
        return [

            'ID',
            'Company Name',
            'Company Mission',
            'Company Vision',
            'Company Type',
            'Number Employee',
            'Company Website',
            'Company Contact',
        ];

    }
}
