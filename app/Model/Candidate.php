<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'candidates';
    protected $fillable = [
        'can_name','can_email','password','phone_num','age','work_experience', 'professional_level', 'education','language', 'hobbies','self_evaluate', 'image','created_at', 'updated_at'
    ];
}
