<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';
    protected $fillable = [
        'job_name','job_description','job_requirement','job_location','job_salary','job_closed', 'cat_id', 'com_id','user_id', 'is_deleted', 'created_at', 'updated_at'
    ];
    public function company(){
        return $this->belongsTo('App\Model\Company', 'com_id');
    }
    public function category(){
        return $this->belongsTo('App\Categories', 'cat_id');
    }
}
