<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CandidateJob extends Model
{
    //
    protected $table = 'candidate_jobs';
    protected $fillable = [
        'id','user_id','job_id','created_at', 'updated_at'
    ];
}
