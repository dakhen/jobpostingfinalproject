<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Carbon;
use PDF;
use PhpWord;
use Illuminate\Support\Facades\Validator;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['userlists'] =  User::all();
        return view('user.user',$data);
    }


    public function saveUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'email' => 'required|unique:users|max:255',
            'pass' => 'required',
          ]);

          if ($validator->fails()) {
            return redirect('user/creatuser')
                     ->withErrors($validator)
                     ->withInput();
          }

        $data = array(
            'name' => $request->username,
            'email' => $request->email,
            'password' => $request->pass,
        );
      
        $i = DB::table('users')->insert($data);
        if($i)
        {
            $request->session()->flash('success', 'User has been saved!');
            return redirect('user/userlist');
        }
        else{
            $request->session()->flash('error','Fail to save user, please check again!');
            return redirect('user/creatuser')->withInput();
        }
    }

    public function creatUser()
    {
        return view('user.creatuser');
    }

    public function editUser($id){

        $data['user'] = User::find($id)
        ->where('id', $id)
        ->first();
        return view('user.updateuser', $data);
    }
    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'email' => 'required',
          ]);

        $data = array(
            'name' => $request->username,
            'email' => $request->email,
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

        $i = User::find($request->id)
            ->update($data);

        if($i)
        {
            $request->session()->flash('success', 'User has been updated!');
            return redirect('user/userlist');
        }
        else{
            $request->session()->flash('error','Fail to update user, please check again!');
            return redirect('user/updateuser')->withInput();
        }
    }

    public function userDetail($id){
        $data['user'] = User::find($id)
        ->where('id', $id)
        ->first();
        return view('user.userdetail', $data);
    }

    public function deleteUser(Request $r)
    {
        User::find($r->id)
            ->delete();
        $r->session()->flash('success', 'User has been removed!');
        return redirect('/user/userlist');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function downloadExcel() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function downloadWord()
    {

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $data = User::all();
        $array_list = '';

        foreach ($data as $key) {
            $array_list .=  $key->id;
            $array_list .=  ' ' .$key->name;
            $array_list .= ' ' .$key->email;
            $text = $section->addText($array_list);
            $array_list = '';

        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('User.docx');
        return response()->download(public_path('User.docx'));

    }
    
    public function downloadPdf()
    {
        // retreive all records from db
      $data['userlists'] = User::all();
      view()->share('user.user',$data);
      $pdf = PDF::loadView('user.pdf_view', $data);
    
      return $pdf->download('user.pdf');
    }
}
