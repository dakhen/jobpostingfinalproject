<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Candidate;
use DB;
use Carbon;
use PDF;
use PhpWord;
use Illuminate\Support\Facades\Validator;

use App\Exports\CandidateExport;
use Maatwebsite\Excel\Facades\Excel;

class CandidateController extends Controller
{
    //ALL CANDIDATE FOR PUBLIC 
    public function index(){
        $data['candidateList'] = Candidate::all();
        return view('candidate.candidate', $data);
    }

    //CREATE CANDIDATE
    public function createcandidate() {
        return view('candidate.createcandidate');
    }

    //ALL CANDIDATE FOR BACK-END 
    public function getAllCandidate(){
        $data['candidateList'] = Candidate::all();
        return view('candidate.candidate', $data);
    }

    //CREATE CANDIDATE
    public function saveCandidate(Request $req){

        $validator = Validator::make($req->all(), [
            'candname' => 'required',
            'canemail' => 'required',
            'phone' => 'required',
            'age' => 'required',
            'edu' => 'required',
            'lang' => 'required',
            'image' => 'required',
          ]);

          if ($validator->fails()) {
            return redirect('candidate/createcandidate')
                     ->withErrors($validator)
                     ->withInput();
          }

        $photoName = time() . '.' . $req->image->getClientOriginalExtension();

        $req->image->move(public_path('photos'), $photoName);

        $data = array(
            'can_name' => $req->candname,
            'can_email' => $req->canemail,
            'password' => 'N/A',
            'phone_num'=> $req->phone,
            'age' => $req->age,
            'work_experience'=> $req->experience,
            'professional_level' => $req->level,
            'education' => $req->edu,
            'language' => $req->lang,
            'hobbies' => $req->hobb,
            'self_evaluate' => $req->eval,
            'image' => 'photos/'.$photoName,
            'user_id' => $req->userid,
            'created_at' => Carbon\Carbon::now()->format('Y-m-d'),
            'updated_at' => Carbon\Carbon::now()->format('Y-m-d'),
        );

        // dd($data);

        $result = DB::table('candidates')->insert($data);
        if($result)
        {
            $req->session()->flash('success', 'Data has been saved!');
            return redirect('candidate/backendcandidate');
        }
        else{
            $req->session()->flash('error','Fail to save data, please check again!');
            return redirect('candidate/createcandidate')->withInput();
        }
    }

    //DETAIL CANDIDATE
    public function detailCandidate($id){
        $data['candidate'] = DB::table('candidates')
        ->where('id', $id)
        ->first();
        // dd($data['candidate']);
        return view('candidate.candidatedetail', $data);
    }

    //EDIT CANDIDATE
    public  function editCandidate($id){

        $data['candidate'] = DB::table('candidates')
        ->where('id', $id)
        ->first();
        return view('candidate.update', $data);
    }

    //PROCESS TO UPDATE CANDIDATE 
    public function updateCandidate(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'candname' => 'required',
            'canemail' => 'required',
            'phone' => 'required',
            'age' => 'required',
            'edu' => 'required',
            'lang' => 'required',
          ]);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

        $candidate = Candidate::find($req->id);
        $candidate->can_name = $req->candname;
        $candidate->can_email = $req->canemail;
        $candidate->password = 'N/A';
        $candidate->phone_num = $req->phone;
        $candidate->age = $req->age;
        $candidate->work_experience = $req->experience;
        $candidate->professional_level = $req->level;
        $candidate->education = $req->edu;
        $candidate->language = $req->lang;
        $candidate->hobbies = $req->hobb;
        $candidate->self_evaluate = $req->eval;
        $candidate->user_id = $req->userid;
        $candidate->updated_at = Carbon\Carbon::now()->format('Y-m-d');

        if ($req->image) {
            $photoName = time() . '.' . $req->image->getClientOriginalExtension();
            $req->image->move(public_path('photos'), $photoName);
            $candidate->image       = 'photos/'.$photoName;
        } else {
            $req->image =  $candidate->image;
        }

        $candidate->save();
        return redirect('candidate/backendcandidate');
    }

    //DELETE CANDIDATE
    public function deleteCandidate(Request $r)
    {
        Candidate::find($r->id)
            ->delete();
        $r->session()->flash('success', 'Data has been removed!');
        return redirect('candidate/backendcandidate');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function downloadExcel() 
    {
        return Excel::download(new CandidateExport, 'Candidate.xlsx');
    }

    public function downloadWord()
    {

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $data = DB::table('candidates')
        ->select('id','can_name','can_email','phone_num','age','work_experience','professional_level','education', 'language')
        ->get();
        $array_list = '';

        foreach ($data as $key) {
            $array_list .=  $key->id;
            $array_list .=  ' ' .$key->can_name;
            $array_list .= ' ' .$key->can_email;
            $array_list .= ' ' .$key->phone_num;
            $array_list .= ' ' .$key->age;
            $array_list .= ' ' .$key->work_experience;
            $array_list .= ' ' .$key->professional_level;
            $array_list .= ' ' .$key->education;
            $array_list .= ' ' .$key->language;
            $text = $section->addText($array_list);
            $array_list = '';

        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Candidate.docx');
        return response()->download(public_path('Candidate.docx'));

    }
    
    public function downloadPdf()
    {
        // retreive all records from db
      $data['candidateList'] = DB::table('candidates')
      ->select('id','can_name','can_email','phone_num','age','work_experience','professional_level','education', 'language')
      ->get();
      view()->share('candidate.candidate',$data);
      $pdf = PDF::loadView('candidate.pdf_view', $data);
    
      return $pdf->download('Candidate.pdf');
    }

}
