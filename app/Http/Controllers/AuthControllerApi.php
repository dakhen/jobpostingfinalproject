<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Model\Job;
use App\Model\Company;
use App\Categories;


class AuthControllerApi extends Controller
{
    //

    public function login(){
        if(Auth::attempt(['email'=> request('email'), 'password' => request('password')])){
            $authenticated_user = Auth::user();
            $user = User::find($authenticated_user->id);
            
            return response()->json(['status' => 'Authorized','data' =>$user], 200);
        }else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
    
    public function register(Request $req){
        $validator = Validator::make($req->all(),
            ['name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
            ]);
        if ($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $req->all();
        $input['password']=bcrypt($input['password']);
        $user = User::create($input);
        $success['name'] = $user->name;
        $success['email'] = $user->email;
        $success['roleid'] = $user->role_id;
        return response()->json(['status'=>'success','data'=>$success], 200);
    }
    
    public function details(){
        $user=Auth::user();

        return response()->json(['success'=>$user],200);
    }

    public function dashboard(){
        $countjobs = Job::count();
        $countcompanies = Company::count();
        $countcategies = Categories::count();
        $countusers = User::count();
        return response()->json(['status' => 'success','data'=>['jobs' =>$countjobs,'companies' =>$countcompanies,'categories' =>$countcategies,'users' =>$countusers]], 200);
    }
}
