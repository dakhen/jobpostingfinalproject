<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FrontController extends Controller
{
    public function index(){
        $data['services'] = DB::table('ourservices')->get();
        return view('layouts/homepage', $data);
    }
}
