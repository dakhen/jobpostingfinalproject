<?php

namespace App\Http\Controllers;

use App\Model\Company;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\FuncCall;
use Illuminate\Support\Facades\Validator;
use PDF;
use PhpWord;
use App\Exports\CompanyExport;
use Maatwebsite\Excel\Facades\Excel;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()     
    {   
        $companies = Company::get();
        // dd($companies);
        
        return view('company.backend.index', compact('companies'));
    }

    public function indexfrontend()
    {   
        $data['companylist'] = DB::table('companies')
        ->select('companies.*')
        ->get();

        $companies['companies'] = Company::all();

        return view('company.frontend.index', $data, $companies);
    }
    // public function companyuser($id){
    //     $data['users'] = DB::table('company')
    //     ->join('user', 'users.id')
    //     ->where('company.id', $id)
    //     ->select('company.*', 'users.*')->get();
    //     return view('company.backend.index', $data);
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.backend.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'company_mission' => 'required',
            'company_vision' => 'required',
            'company_type' => 'required',
            'employee' => 'required',
            'company_website' => 'required',
            'company_contact' => 'required',
            'image' => 'required',
          ]);

          if ($validator->fails()) {
            return redirect('company/create')
                     ->withErrors($validator)
                     ->withInput();
          }

        $companies = new Company();

        $companies->company_name = $request->company_name;
        $companies->company_mission = $request->company_mission;
        $companies->company_vision = $request->company_vision;
        $companies->company_type = $request->company_type;
        $companies->company_emp_num = $request->employee;
        $companies->company_website = $request->company_website;
        $companies->company_contact = $request->company_contact;

        $photoName = time() . '.' . $request->image->getClientOriginalExtension();

        $request->image->move(public_path('photos'), $photoName);

        $companies->image = 'photos/'.$photoName;
        $companies->user_id = Auth::user()->id;

        $companies->save();
        return redirect()->route('company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companies = Company::findOrFail($id);
        return view('company.backend.show', compact('companies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Company::find($id)
        ->where('id', $id)
        ->first();
        return view('company.backend.edit', compact('companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'company_mission' => 'required',
            'company_vision' => 'required',
            'company_type' => 'required',
            'employee' => 'required',
            'company_website' => 'required',
            'company_contact' => 'required',
          ]);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

        $companies = Company::find($id);
        $companies->company_name = $request->company_name;
        $companies->company_mission = $request->company_mission;
        $companies->company_vision = $request->company_vision;
        $companies->company_type = $request->company_type;
        $companies->company_emp_num = $request->employee;
        $companies->company_website = $request->company_website;
        $companies->company_contact = $request->company_contact;

        if ($request->image) {
                $photoName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('photos'), $photoName);
                $companies->image       = 'photos/'.$photoName;
            } else {
                $request->image =  $companies->image;
            }

        $companies->save();
        return redirect()->route('company.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $companies = Company::find($id);
        $companies->delete();
        return redirect()->route('company.index');
    }

    public function searchCompany()
    {
        $keyword = $_GET['keyword'];
        $company = $_GET['company'];

        $data['companylist'] = DB::table('companies')
        ->select('companies.*')
        ->where([ 
            ['company_name', 'LIKE', '%' . $keyword . '%'],
            ['company_name', 'LIKE', '%' . $company . '%'],
        ])
        ->get();

        $companies['companies'] = Company::all();

        return view('company.frontend.index', $data, $companies);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function downloadExcel() 
    {
        return Excel::download(new CompanyExport, 'Company.xlsx');
    }

    public function downloadWord()
    {

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $data = Company::all();
        $array_list = '';

        foreach ($data as $key) {
            $array_list .=  $key->id;
            $array_list .=  ' ' .$key->company_name;
            $array_list .= ' ' .$key->company_mission;
            $array_list .= ' ' .$key->company_vision;
            $array_list .= ' ' .$key->company_type;
            $array_list .= ' ' .$key->company_emp_num;
            $array_list .= ' ' .$key->company_website;
            $array_list .= ' ' .$key->company_contact;
            $text = $section->addText($array_list);
            $array_list = '';

        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Company.docx');
        return response()->download(public_path('Company.docx'));

    }
    
    public function downloadPdf()
    {
        // retreive all records from db
      $data['companies'] = Company::all();
      view()->share('company.index',$data);
      $pdf = PDF::loadView('company.backend.pdf_view', $data);
    
      return $pdf->download('Company.pdf');
    }
}
