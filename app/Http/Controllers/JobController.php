<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Job;
use App\Model\Company;
use App\Categories;
use DB;
use Carbon;
use PDF;
use PhpWord;
use App\Exports\JobExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class JobController extends Controller
{
    public function index(){
        $categories['categories'] = Categories::all();
        $data['joblists'] = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->select('companies.*','categories.*', 'jobs.*')
        ->get();

        return view('job.job', $data, $categories);

    }
    
    // for frontend process
    public function getAllJobs(){
        $data['joblists'] = Job::all();
        return view('job.jobbackend', $data);
    }

    public function createJob(){
        $data['categories'] = DB::table('categories')->get();
        $data['companies'] = DB::table('companies')->get();
        return view('job.createjob',$data);
    }

    public function saveJob(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'JobName'=>'required',
            'JobDescription'=>'required',
            'JobRequirement'=>'required',
            'JobLocation'=>'required',
            'JobSalary'=>'required',
            'JobCloseDate'=>'required',
            'Category'=>'required',
            'Company'=>'required',
          ]);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

        $data = array(
            'job_name' => $request->JobName,
            'job_description' => $request->JobDescription,
            'job_requirement' => $request->JobRequirement,
            'job_location'=> $request->JobLocation,
            'job_salary' => $request->JobSalary,
            'job_closed'=> $request->JobCloseDate,
            'cat_id'=>$request->Category,
            'com_id'=>$request->Company,
            'user_id'=> $request->userid,
            'created_at' => Carbon\Carbon::now()->format('Y-m-d'),
            'updated_at' => Carbon\Carbon::now()->format('Y-m-d'),
        );
      
        $i = DB::table('jobs')->insert($data);
        if($i)
        {
            $request->session()->flash('success', 'Job has been saved!');
            return redirect('job/backendjobs');
        }
        else{
            $request->session()->flash('error','Fail to save Job, please check again!');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    public function viewDetailJob($id){
        $data['job'] = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->where('jobs.id', $id)
        ->select('jobs.*', 'companies.*','categories.*')
        ->first();

        return view('job.jobdetailfront', $data);
    }

    public function detailJob($id){
        $data['job'] = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->where('jobs.id', $id)
        ->select('jobs.*', 'companies.*','categories.*')
        ->first();
        return view('job.jobdetail', $data);
    }

    public  function editJob($id){
        $data['categories'] = DB::table('categories')
            ->get();
        $data['companies'] = DB::table('companies')
            ->get();

        $data['job'] = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->where('jobs.id', $id)
        ->select('jobs.*','companies.id as comid', 'categories.id as catid')
        ->first();
        return view('job.update', $data);
    }

    public function updateJob(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'JobName'=>'required',
            'JobDescription'=>'required',
            'JobRequirement'=>'required',
            'JobLocation'=>'required',
            'JobSalary'=>'required',
            'JobCloseDate'=>'required',
            'Category'=>'required',
            'Company'=>'required',
          ]);

          $data = array(
            'job_name' => $request->JobName,
            'job_description' => $request->JobDescription,
            'job_requirement' => $request->JobRequirement,
            'job_location'=> $request->JobLocation,
            'job_salary' => $request->JobSalary,
            'job_closed'=> $request->JobCloseDate,
            'cat_id'=>$request->Category,
            'com_id'=>$request->Company,
            'user_id'=> $request->userid,
            'created_at' => Carbon\Carbon::now()->format('Y-m-d'),
            'updated_at' => Carbon\Carbon::now()->format('Y-m-d'),
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

          $i = Job::find($request->jobid)
          ->update($data);

        if($i)
        {
            $request->session()->flash('success', 'Job has been updated!');
            return redirect('job/backendjobs');
        }
        else{
            $request->session()->flash('error','Fail to update job, please check again!');
            return redirect('job.update')->withInput();
        }

    }

    public function deleteJob(Request $r)
    {
        Job::find($r->id)
            ->delete();
        $r->session()->flash('success', 'Data has been removed!');
        return redirect('job/backendjobs');
    }

    public function searchjob()
    {
        $position = $_GET['position'];
        $category = $_GET['category'];

        $data['joblists'] = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->select('jobs.*', 'companies.*','categories.*')
        ->where([ 
            ['jobs.job_name', 'LIKE', '%' . $position . '%'],
            ['jobs.cat_id', 'LIKE', '%' . $category . '%'],
        ])
        ->get();

        $categories['categories'] = Categories::all();

        return view('job.job', $data, $categories);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function downloadExcel() 
    {
        return Excel::download(new JobExport, 'Job.xlsx');
    }

    public function downloadWord()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $data = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->select('jobs.id','job_name','job_description','job_requirement','job_location','job_salary','job_closed','cat_name', 'company_name')
        ->get();

        $array_list = '';

        foreach ($data as $key) {
            $array_list .=  $key->id;
            $array_list .=  ' ' .$key->job_name;
            $array_list .= ' ' .$key->job_description;
            $array_list .= ' ' .$key->job_requirement;
            $array_list .= ' ' .$key->job_location;
            $array_list .= ' ' .$key->job_salary;
            $array_list .= ' ' .$key->job_closed;
            $array_list .= ' ' .$key->cat_name;
            $array_list .= ' ' .$key->company_name;
            $text = $section->addText($array_list);
            $array_list = '';

        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Job.docx');
        return response()->download(public_path('Job.docx'));

    }
    
    public function downloadPdf()
    {
        $data['joblists'] = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->select('jobs.id','job_name','job_description','job_requirement','job_location','job_salary','job_closed','cat_name', 'company_name')
        ->get();
      view()->share('job.jobbackend',$data);
      $pdf = PDF::loadView('job.pdf_view', $data);
    
      return $pdf->download('Job.pdf');
    }

}
