<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\FeedbackReceived;
use App\Mail\Replyback;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use PDF;
use PhpWord;
use App\Exports\CategoryExport;
use Maatwebsite\Excel\Facades\Excel;


class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::all();
        return view('categories.index', compact('categories', $categories));
    }
    public function indexfrontend()
    {
        $data['categorylist'] = Categories::all();
        $categories['categories'] = Categories::all();
        
        return view('Categories.frontend.index', $data, $categories);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cat_name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data = array(
            'cat_name' => $request->cat_name,
            'created_at' => Carbon::now()->format('Y-m-d'),
        );
      
        $i = DB::table('categories')->insert($data);
        if($i)
        {
            $request->session()->flash('success', 'Category has been saved!');
            return redirect('backend/user/category/categories');
        }
        else{
            $request->session()->flash('error','Fail to save category, please check again!');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Categories $category)
    {
        return view('categories.show', compact('category', $category));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categories  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Categories $category)
    {
        return view('categories.edit', compact('category', $category));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categories $category)
    {
        $validator = Validator::make($request->all(), [
            'cat_name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data = array(
            'cat_name' => $request->cat_name,
            'updated_at' => Carbon::now()->format('Y-m-d'),
        );

        $i = Categories::find($category->id)
            ->update($data);

        if($i)
        {
            $request->session()->flash('success', 'Successfully modified the categories!');
            return redirect('backend/user/category/categories');
        }
        else{
            $request->session()->flash('error','Fail to update Category, please check again!');
            return redirect()->back()->withErrors($validator)->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Categories $category)
    {
        $category->delete();
        $request->session()->flash('message', 'Successfully deleted the The Category!');
        return redirect('backend/user/category/categories');
    }

    public function searchCategory()
    {
        $keyword = $_GET['keyword'];
        $category = $_GET['category'];

        $data['categorylist'] = DB::table('categories')
        ->select('categories.*')
        ->where([ 
            ['cat_name', 'LIKE', '%' . $keyword . '%'],
            ['id', 'LIKE', '%' . $category . '%'],
        ])
        ->get();

        $categories['categories'] = Categoriess::all();

        return view('Categories.frontend.index', $data, $categories);
    }

     /**
    * @return \Illuminate\Support\Collection
    */
    public function downloadExcel() 
    {
        return Excel::download(new CategoryExport, 'Category.xlsx');
    }

    public function downloadWord()
    {

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $data = Categories::all();
        $array_list = '';

        foreach ($data as $key) {
            $array_list .=  $key->id;
            $array_list .=  ' ' .$key->cat_name;
            $array_list .= ' ' .$key->created_at;
            $array_list .= ' ' .$key->updated_at;
            $text = $section->addText($array_list);
            $array_list = '';

        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Category.docx');
        return response()->download(public_path('Category.docx'));

    }
    
    public function downloadPdf()
    {
        // retreive all records from db
      $data['categories'] = Categories::all();
      view()->share('Categories.index',$data);
      $pdf = PDF::loadView('Categories.pdf_view', $data);
    
      return $pdf->download('Category.pdf');
    }
}
