<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Model\Candidate;
use App\Model\Company;
use Illuminate\Http\Request;
use App\Model\Job;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $jobs = DB::table('Job')->count();
        $jobs = Job::count();
        $companies = Company::count();
        $categories = Categories::count();
        $candidate = Candidate::count();

        return view('dashboard', compact('jobs', 'companies', 'categories', 'candidate'));
    }
}
