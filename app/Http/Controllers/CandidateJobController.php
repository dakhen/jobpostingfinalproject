<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon; 

class CandidateJobController extends Controller
{
    //
    public function applyJob(Request $r)
    {
        // $data = array(
        //     'can_id'=> $r->userid
        // );
        $values = array(
            'user_id'=> $r->userid,
            'job_id'=>$r->id,
            'created_at'=>Carbon::now()->format('Y-m-d'),
            'updated_at'=>Carbon::now()->format('Y-m-d'),
        );

        $userid = $r->userid;
        $i = DB::table('candidate_jobs')->insert($values);
        
        if($i)
        {
            $r->session()->flash('success', 'Job Applied Succesfully!');
            return redirect('job/backendjobs');
        }
        else{
            $r->session()->flash('error','Fail to apply, please check again!');
            return redirect('job/backendjobs');
        }
    }

    public function candidatesAppliedJob($id){
        $data['allcandidates'] = DB::table('candidate_jobs')
        ->join('users', 'candidate_jobs.user_id', 'users.id')
        ->join('jobs', 'candidate_jobs.job_id', 'jobs.id')
        ->join('candidates', 'candidates.user_id', 'users.id')
        ->where('jobs.id', $id)
        ->select('jobs.*', 'users.*','candidates.*')->get();
        // dd($data['allcandidates']);
        return view('job.candidateappliedjob', $data);
    }
}
