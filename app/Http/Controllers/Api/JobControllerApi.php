<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\CandidateJob;
use DB;
use Carbon\Carbon; 
use App\Model\Job;


class JobControllerApi extends Controller
{
    //
    public function show(){
        $jobs = Job::all();
        if($jobs != Null && $jobs != 'empty'){
            return response()->json(['status' => 'success','data'=>$jobs], 200);
        }else {
            return response()->json(['status' => 'no recoreds!!'], 403);
        }
    }

    public function jobDetail($id)
    {
        // $job = Job::findOrFail($id);
        $job = DB::table('jobs')
        ->join('companies', 'jobs.com_id', 'companies.id')
        ->join('categories', 'jobs.cat_id', 'categories.id')
        ->where('jobs.id', $id)
        ->select('jobs.*', 'companies.company_name as companyname','categories.cat_name as categoryname')
        ->first();

        if($job != Null && $job != 'empty'){
            return response()->json(['status' => 'success','data'=>$job], 200);
        }else {
            return response()->json(['status' => 'no record found!!'], 403);
        }
    }

    public function appyJob(Request $req){
        $validator = Validator::make($req->all(),
            ['user_id' => 'required',
            'job_id' => 'required'
            ]);
        if ($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input['created_at'] = Carbon::now()->format('Y-m-d');
        $input['updated_at'] = Carbon::now()->format('Y-m-d');
        $input = $req->all();
        $user = CandidateJob::create($input);
        return response()->json(['status'=>'Job Apply Successful!!'], 200);
    }
}
