<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Model\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use PDF;
use PhpWord;
use App\Exports\RoleExport;
use Maatwebsite\Excel\Facades\Excel;


class RoleController extends Controller
{
    public function index()
    {
        $data['rolelists'] =  Role::all();
        return view('role.index',$data);
    }
    

    public function creatRole()
    {
        return view('role.createrole');
    }

    public function saveRole(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rolename' => 'required',
          ]);

          if ($validator->fails()) {
            return redirect('role/createrole')
                     ->withErrors($validator)
                     ->withInput();
          }

        $data = array(
            'role_name' => $request->rolename,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        );
      
        $i = DB::table('roles')->insert($data);
        if($i)
        {
            $request->session()->flash('success', 'Role has been saved!');
            return redirect('role/rolelist');
        }
        else{
            $request->session()->flash('error','Fail to save role, please check again!');
            return redirect('role/createrole')->withInput();
        }
    }


    public function editRole($id){
        $data['role'] = Role::find($id)
        ->where('id', $id)
        ->first();
        return view('role.updaterole', $data);
    }
    public function updateRole(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rolename' => 'required',
          ]);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

        $data = array(
            'role_name' => $request->rolename,
            'updated_at' => Carbon::now(),
        );

        $i = Role::find($request->id)
            ->update($data);

        if($i)
        {
            $request->session()->flash('success', 'User has been saved!');
            return redirect('role/rolelist');
        }
        else{
            $request->session()->flash('error','Fail to save user, please check again!');
            return redirect('role/createrole')->withInput();
        }
    }

    public function deleteRole(Request $r)
    {
        Role::find($r->id)
            ->delete();
        $r->session()->flash('success', 'Role has been removed!');
        return redirect('role/rolelist');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function downloadExcel() 
    {
        return Excel::download(new RoleExport, 'Role.xlsx');
    }

    public function downloadWord()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $data = Role::all();
        $array_list = '';

        foreach ($data as $key) {
            $array_list .=  $key->id;
            $array_list .=  ' ' .$key->role_name;
            $array_list .= ' ' .$key->created_at;
            $array_list .= ' ' .$key->updated_at;
            $text = $section->addText($array_list);
            $array_list = '';

        }

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Role.docx');
        return response()->download(public_path('Role.docx'));

    }
    
    public function downloadPdf()
    {
      $data['rolelists'] = Role::all();
      view()->share('role.index',$data);
      $pdf = PDF::loadView('role.pdf_view', $data);
    
      return $pdf->download('Role.pdf');
    }
}
