<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', "FrontController@index");

Auth::routes();
Route::get('candidateregister','Auth\RegisterController@loadForm');
Route::post('saveregister','Auth\RegisterController@createCandidate');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/homepage', 'FrontController@index');

// Route::resource('/company', 'CompanyController');
Route::get('/company', 'CompanyController@index')->name('company.index');
Route::get('/company-show/{id}', 'CompanyController@show')->name('company.show');
Route::get('/company/create', 'CompanyController@create')->name('company.create');
Route::post('/ads-create-action', 'CompanyController@store')->name('company.store');
Route::get('/company-edit/{id}', 'CompanyController@edit')->name('company.edit');
Route::put('/company-update-action/{id}', 'CompanyController@update')->name('company.update');
Route::get('/company-delete/{id}', 'CompanyController@destroy')->name('company.destroy');
Route::get('/companylist', 'CompanyController@indexfrontend');

Route::prefix('company')->group(function(){
    Route::get('searchCompany', 'CompanyController@searchCompany');
    Route::get('exportexcel', 'CompanyController@downloadExcel');
    Route::get('exportword', 'CompanyController@downloadWord');
    Route::get('exportpdf', 'CompanyController@downloadPdf');

});
// =========job crud========
Route::prefix('job')->group(function(){
    Route::get('alljobs', 'JobController@index');
    Route::get('backendjobs','JobController@getAllJobs');
    Route::get('createjob','JobController@createJob');
    // =====for front-end======
    Route::get('viewdetailjob/{id}', 'JobController@viewDetailJob');
    Route::get('detailjob/{id}', 'JobController@detailJob');
    Route::post('savejob', 'JobController@saveJob');
    Route::get('editjob/{id}', 'JobController@editJob');
    Route::put('update', 'JobController@updateJob');
    Route::get('deletejob', 'JobController@deleteJob');
    Route::get('applyjob', 'CandidateJobController@applyJob');
    Route::get('allappliedjob/{id}', 'CandidateJobController@candidatesAppliedJob');
    Route::get('searchjob', 'JobController@searchjob');
    Route::get('exportexcel', 'JobController@downloadExcel');
    Route::get('exportword', 'JobController@downloadWord');
    Route::get('exportpdf', 'JobController@downloadPdf');
});

Route::prefix('candidate')->group(function(){
    Route::get('allcandidate','CandidateController@index'); //for PUBLIC USER
    Route::get('createcandidate','CandidateController@createcandidate');
    Route::post('saveCandidate', 'CandidateController@saveCandidate');
    Route::get('backendcandidate','CandidateController@getAllCandidate');
    Route::get('detailCandidate/{id}', 'CandidateController@detailCandidate');
    Route::get('editCandidate/{id}', 'CandidateController@editCandidate');
    Route::post('update', 'CandidateController@updateCandidate');
    Route::get('deleteCandidate', 'CandidateController@deleteCandidate');
    Route::get('exportexcel', 'CandidateController@downloadExcel');
    Route::get('exportword', 'CandidateController@downloadWord');
    Route::get('exportpdf', 'CandidateController@downloadPdf');

});

// =========user crud=============
Route::prefix('user')->group(function(){
    Route::get('userlist','UserController@index');
    Route::get('creatuser', 'UserController@creatUser');
    Route::post('saveuser', 'UserController@saveUser');
    Route::get('userdetail/{id}', 'UserController@userDetail');

    Route::get('edit/{id}', 'UserController@editUser');
    Route::put('updateuser', 'UserController@updateUser');
    Route::get('deleteUser', 'UserController@deleteUser');
    Route::get('exportexcel', 'UserController@downloadExcel');
    Route::get('exportword', 'UserController@downloadWord');
    Route::get('exportpdf', 'UserController@downloadPdf');

});

// =========role crud=============
Route::prefix('role')->group(function(){
    Route::get('rolelist','RoleController@index');
    Route::get('createrole', 'RoleController@creatRole');
    Route::post('saverole', 'RoleController@saveRole');
    Route::get('editrole/{id}', 'RoleController@editRole');
    Route::post('updaterole', 'RoleController@updateRole');
    Route::get('deleterole', 'RoleController@deleteRole');
    Route::get('exportexcel', 'RoleController@downloadExcel');
    Route::get('exportword', 'RoleController@downloadWord');
    Route::get('exportpdf', 'RoleController@downloadPdf');

});
//=========category crud===========

Route::resource('backend/user/category/categories', 'CategoriesController');
Route::prefix('category')->group(function(){
    Route::get('categorylist', 'CategoriesController@indexfrontend');
    Route::get('searchCategory', 'CategoriesController@searchCategory');
    Route::get('exportexcel', 'CategoriesController@downloadExcel');
    Route::get('exportword', 'CategoriesController@downloadWord');
    Route::get('exportpdf', 'CategoriesController@downloadPdf');
});

//========contact_us
// Render in view
Route::get('/contact', [
    'uses' => 'ContactUsFormController@createForm'
]);

// Post form data
Route::post('/contact', [
    'uses' => 'ContactUsFormController@ContactUsForm',
    'as' => 'contact.store'
]);

